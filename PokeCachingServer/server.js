// Load the TCP Library
net = require('net');
https = require('https');
var fs = require('fs');

var options = {
  key: fs.readFileSync('key.pem'),
  cert: fs.readFileSync('cert.pem')
};

var db_manager = require("./DatabaseManager");
var pk_manager = require("./PacketManager");
var JsonSocket = require('json-socket');
 
// Keep track of the clients
var clients = [];

var Users = [];
var Locations = [];
var Items = [];
var Pokemons = [];

db_manager.LoadDatabase(Locations, Pokemons, Items, Users);

https.createServer(options,function(req,res) {
	var body = "";
	req.on('data',function(data) {
		body += data;
	});
	
	req.on('end',function() {
		pk_manager.ParseRequest(res,req,body,Users,Pokemons,Items,Locations);
	});
}).listen(5000);
 
// Put a friendly message on the terminal of the server.
console.log("Server running at port 5000\n");