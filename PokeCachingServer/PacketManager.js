//
//	PACKET MANAGER MODULE
//	Provides methods to parse and send packets
//

var db_manager = require("./DatabaseManager");

//PACKET PARSING

exports.ParseRequest = function(response, request, body, users, pokemons, items, locations) {
	var error;
	var packet = null;
	try {
		packet = JSON.parse(body);
	} catch(e) {
		error = "{\"error\" : \"Incorrect message type.\"}";
		response.writeHead(400,{ 'Content-Type': 'application/json',
					  'Content-Length': error.length });
		response.end(error);
		return;
	}
	 
	console.log(body);
	if(request.method == "GET") {
		if(packet.type == "GET_POKEMON")
			ParseGetPokemon(response, packet, pokemons);
		else if(packet.type == "AUTHENTICATE_LOGIN")
			ParseLogin(response, packet, users);
		else if(packet.type == "GET_PARTY_POKEMON_INFO")
			ParseGetParty(response, packet, users);
		else if(packet.type == "GET_POKEDEX")
			ParseGetPokedex(response, packet, pokemons);
		else if(packet.type == "GET_MAP_POKEMON_INFO")
			ParseGetMapPokemon(response, packet, locations);
		else if(packet.type == "GET_USER_ITEMS")
			ParseGetUserItems(response, packet, users);
		else if(packet.type == "GET_ITEMS")
			ParseGetItems(response, packet, items);
		else {
			error = "{\"error\" : \"Incorrect message type.\"}";
			response.writeHead(400,{ 'Content-Type': 'application/json',
                          'Content-Length': error.length });
			response.end(error);
		}
	}
	else if(request.method == "PUT") {
		if(packet.type == "REGISTER")
			ParseRegister(response, packet, users);
		else if(packet.type == "STORE_POKEMON")
			ParseStorePokemon(response, packet, users, pokemons);
		else {
			error = "{\"error\" : \"Incorrect message type.\"}";
			response.writeHead(400,{ 'Content-Type': 'application/json',
                          'Content-Length': error.length });
			response.end(error);
		}
	}
	else if(request.method == "POST") {
		if(packet.type == "CHANGE_ITEMS")
			ParseChangeItems(response, packet, users, items);
		else {
			error = "{\"error\" : \"Incorrect message type.\"}";
			response.writeHead(400,{ 'Content-Type': 'application/json',
                          'Content-Length': error.length });
			response.end(error);
		}
	}
	else if(request.method == "DELETE") {
		if(packet.type == "REMOVE_POKEMON")
			ParseRemovePokemon(response, packet, users);
		else {
			error = "{\"error\" : \"Incorrect message type.\"}";
			response.writeHead(400,{ 'Content-Type': 'application/json',
                          'Content-Length': error.length });
			response.end(error);
		}
	}
	else {
		error = "{\"error\" : \"Unrecognize message.\"}";
		response.writeHead(401,{ 'Content-Type': 'application/json',
                          'Content-Length': error.length });
		response.end(error);
	}
}

//Parses packet info and calls SendPokemon
function ParseGetPokemon(response, packet, pokemons) {
	if(packet.id != undefined)
		SendPokemon(response,parseInt(packet.id),pokemons);
}

//Parses packet info, calls db_manager.RegisterUser and then SendCommandResponse
function ParseRegister(response, packet, users) {

	if(packet.password != undefined && packet.username != undefined) {
		db_manager.RegisterUser(packet.username, packet.password, users, function(result) {
			if(result == true)
				SendCommandResponse(response, true);
			else
				SendCommandResponse(response, false);
		})
	}
}

//Parses packet info, calls db_manager.LoginUser and then SendCommandResponse
function ParseLogin(response, packet, users) {
	if(packet.username != undefined && packet.password != undefined) {
		db_manager.LoginUser(packet.username, packet.password, users, function(result) {
			if(result == true)
				SendCommandResponse(response, true);
			else
				SendCommandResponse(response, false);
		});
	}
}

//Parses packet info and calls SendPartyPokemon
function ParseGetParty(response, packet, users) {
	if(packet.username != undefined) {
		var user = null;
		
		for(var i = 0; i < users.length; i++) {
			if(users[i].username === packet.username)
				user = users[i];
		}
		
		if(user != null)
			SendPartyPokemon(response, user);
	}
}

//Parses packet info and calls SendPokedex
function ParseGetPokedex(response, packet, pokemons) {
	SendPokedex(response, pokemons);
}

//Parses packet info and calls SendMapPokemon
function ParseGetMapPokemon(response, packet, locations) {
	if(packet.latitude != undefined && packet.longitude != undefined) {
		SendMapPokemon(response, parseFloat(packet.latitude), parseFloat(packet.longitude), locations);
	}
}

//Parses packet info and calls SendUserItems
function ParseGetUserItems(response, packet, users) {

	if(packet.username != undefined) {
		var user = null;
		
		for(var i = 0; i < users.length; i++) {
			if(users[i].username === packet.username)
				user = users[i];
		}
		
		if(user != null)
			SendUserItems(response, user);
	}
}

//Parses packet info and calls SendItems
function ParseGetItems(response, packet, items) {
	SendItems(response, items);
}

//Parses packet info, calls db_manager.RegisterPokemon and then SendCommandResponse
function ParseStorePokemon(response, packet, users, pokemons) {
	if(packet.username != undefined && packet.id != undefined && packet.nickname != undefined && packet.date != undefined) {
		db_manager.RegisterPokemon(packet.username, packet.id, packet.date, packet.nickname, users, pokemons, function(result) {
			if(result == true)
				SendCommandResponse(response, true);
			else
				SendCommandResponse(response, false);
		})
	}
}

//Parses packet info, calls db_manager.UnregisterPokemon and then SendCommandResponse
function ParseRemovePokemon(response, packet, users) {
	if(packet.id != undefined) {
		db_manager.UnregisterPokemon(packet.id, users, function(result) {
			if(result == true)
				SendCommandResponse(response, true);
			else
				SendCommandResponse(response, false);
		})
	}
}

//Parses packet info, calls db_manager.UpdateUserItems and then SendCommandResponse
function ParseChangeItems(response, packet, users, items) {
	if(packet.username != undefined && packet.id != undefined && packet.quantity != undefined && packet.add != undefined) {
		db_manager.UpdateUserItems(packet.username, packet.id, packet.quantity, packet.add, users, items, function(result) {
			if(result == true)
				SendCommandResponse(response, true);
			else
				SendCommandResponse(response, false);
		})
	}
}

//Parses packet info, calls db_manager.RemoveNotification and then SendCommandResponse
function ParseDeleteNotification(response, packet, users) {
}

//POSSIBLY ADD MORE

//PACKET SENDING

function SendCommandResponse(response, success) {
	var packet;
	
	if(success)
		packet = "{\"type\" : \"COMMAND_RESPONSE\", \"success\" : \"yes\"}";
	else
		packet = "{\"type\" : \"COMMAND_RESPONSE\", \"success\" : \"no\"}";
	response.writeHead(200,{ 'Content-Type': 'application/json',
                          'Content-Length': packet.length });
	response.end(packet);
	
	console.log("Sent COMMAND_RESPONSE");
}

function SendMapPokemon(response, latitude, longitude, locations) {
	var toSend = [];
	
	for(var i = 0; i < locations.length; i++) {
		var distance = Math.abs(longitude - locations[i].longitude) + Math.abs(latitude - locations[i].latitude);
		if(distance < 0.1)
			toSend.push(locations[i]);
	}
	
	var packet = { "type" : "SEND_MAP_POKEMON", "pokemons" : []};
	
	for(var i = 0; i < toSend.length; i++) {
		var pokemon = { "id" : 0, "longitude" : 0.0, "latitude" : 0.0, "radius" : 0, "weather" : "", "timeStart" : 0, "timeEnd" : 0, "sprite" : ""};
		pokemon.id = toSend[i].pokemon.id;
        pokemon.longitude = toSend[i].longitude;
		pokemon.latitude = toSend[i].latitude;
		pokemon.weather = toSend[i].pokemon.weather_preference;
		pokemon.timeStart = toSend[i].pokemon.time_preference.start_time;
		pokemon.timeEnd = toSend[i].pokemon.time_preference.end_time;
		pokemon.sprite = toSend[i].pokemon.sprite_small;
		pokemon.radius = toSend[i].radius;
		packet.pokemons.push(pokemon);
	}
	
	var string = JSON.stringify(packet);
	response.writeHead(200,{ 'Content-Type' : 'application/json', 'Content-Length' : string.length });
	response.end(string);
	console.log("Sent SEND_MAP_POKEMON");
}

function SendPokedex(response, pokemons) {
	var packet = { "type" : "SEND_POKEDEX", "pokemons" : []};
	
	for(var i = 0; i < pokemons.length; i++) {
		var pokemon = { "id" : 0, "name" : "", "sprite" : "", "timeStart" : 0, "timeEnd" : 0, "weather" : ""};
		pokemon.id = pokemons[i].id;
        pokemon.name = pokemons[i].name;
		pokemon.sprite = pokemons[i].sprite_small;
		pokemon.timeStart = pokemons[i].time_preference.start_time;
		pokemon.timeEnd = pokemons[i].time_preference.end_time;
		pokemon.weather = pokemons[i].weather_preference;
		packet.pokemons.push(pokemon);
	}
	
	var string = JSON.stringify(packet);
	response.writeHead(200,{ 'Content-Type' : 'application/json', 'Content-Length' : string.length });
	response.end(string);
	console.log("Sent SEND_POKEDEX");
}

function SendPokemon(response, pokemonId, pokemons) {
	var pokemon = null;
	for(var i = 0; i < pokemons.length; i++) {
		if(pokemonId === pokemons[i].id)
			pokemon = pokemons[i];
	}

	if(pokemon != null) {
		var packet = {"type" : "SEND_POKEMON", "id" : 0, "name" : "", "description" : "", "strength" : 0, "spriteSmall" : "", "spriteNormal" : "", "timeStart" : 0, "timeEnd" : 0, "weather" : ""};
		packet.type = "SEND_POKEMON";
		
		packet.id = pokemon.id;
		packet.name = pokemon.name;
		packet.description = pokemon.description;
		packet.strength = pokemon.strenght;
		packet.weather = pokemon.weather_preference;
		packet.timeStart = pokemon.time_preference.start_time;
		packet.timeEnd = pokemon.time_preference.end_time;
		packet.spriteSmall = pokemon.sprite_small;
		packet.spriteNormal = pokemon.sprite_normal;
	}
	
	var string = JSON.stringify(packet);
	response.writeHead(200,{ 'Content-Type' : 'application/json', 'Content-Length' : string.length });
	response.end(string);
	console.log("Sent SEND_POKEMON");
}

function SendItems(response, items) {
	var packet = {"type" : "SEND_ITEMS", "items" : []};
	
	for(var i = 0; i < items.length; i++) {
		var item = { "id" : 0, "name" : "", "sprite" : "", "strength" : 0, "description" : "", "free" : false, "respawnTime" : null, "price" : 0};
		item.id = items[i].id;
        item.name = items[i].name;
		item.sprite = items[i].sprite;
		item.strength = items[i].strength;
		item.description = items[i].description;
		if(items[i].free)
			item.free = "true";
		else
			item.free = "false";
		item.respawnTime = items[i].respawn_time;
		item.price = items[i].price;
		packet.items.push(item);
	}
	
	var string = JSON.stringify(packet);
	response.writeHead(200,{ 'Content-Type' : 'application/json', 'Content-Length' : string.length });
	response.end(string);
	console.log("Sent SEND_ITEMS");
}

function SendPartyPokemon(response, user) {
	var packet = {"type" : "SEND_PARTY_POKEMON", "pokemons" : []};
	
	for(var i = 0; i < user.pokemons.length; i++) {
		var pokemon = { "id" : 0, "pokemonId" : 0, "nickname" : "", "dateCaught" : ""};
		pokemon.id = user.pokemons[i].id;
		pokemon.pokemonId = user.pokemons[i].pokemon.id;
		pokemon.nickname = user.pokemons[i].nickname;
		pokemon.dateCaught = user.pokemons[i].catchDate;
		packet.pokemons.push(pokemon);
	}
	
	var string = JSON.stringify(packet);
	response.writeHead(200,{ 'Content-Type' : 'application/json', 'Content-Length' : string.length });
	response.end(string);
	console.log("Sent SEND_PARTY_POKEMON");
}

function SendUserItems(response, user) {
	var packet = {"type" : "SEND_USER_ITEMS", "items" : []};
	
	for(var i = 0; i < user.items.length; i++) {
		var item = {"id" : 0, "quantity": 0};
		item.id = user.items[i].item.id;
		item.quantity = user.items[i].quantity;
		packet.items.push(item);
	}
	
	var string = JSON.stringify(packet);
	response.writeHead(200,{ 'Content-Type' : 'application/json', 'Content-Length' : string.length });
	response.end(string);
	console.log("Sent SEND_USER_ITEMS");
}

exports.SendNotification = function(response, user, id  , header, body, sprite, date) {
	var packet = {"type" : "SEND_NOTIFICATION", "id" : 0, "header" : "", "body": "", "sprite" : "", "date" : ""};
	packet.id = id;
	packet.header = header;
	packet.body = body;
	packet.sprite = sprite;
	packet.date = date;
	
	var string = JSON.stringify(packet);
	response.writeHead(200,{ 'Content-Type' : 'application/json', 'Content-Length' : string.length });
	response.end(string);
	console.log("Sent SEND_NOTIFICATION");
}

