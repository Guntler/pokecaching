DROP TABLE IF EXISTS pokemon;
DROP TABLE IF EXISTS app_user;
DROP TABLE IF EXISTS pokeball;
DROP TABLE IF EXISTS pokemon_user;
DROP TABLE IF EXISTS pokeball_user;
DROP TABLE IF EXISTS location;
DROP TABLE IF EXISTS weather;
DROP TABLE IF EXISTS time_of_day;
DROP TABLE IF EXISTS notification;

CREATE TABLE pokemon (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	name TEXT UNIQUE NOT NULL,
	description TEXT NOT NULL,
	strength INTEGER NOT NULL,
	weather_preference INTEGER REFERENCES weather(id),
	time_preference INTEGER REFERENCES time_of_day(id),
	sprite_small TEXT NOT NULL,
	sprite_normal TEXT NOT NULL,
	CHECK(strength <= 100 AND strength >= 0)
);

CREATE TABLE app_user (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	username TEXT UNIQUE NOT NULL,
	password TEXT NOT NULL
);

CREATE TABLE pokeball (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	name TEXT UNIQUE NOT NULL,
	description TEXT NOT NULL,
	strength INTEGER NOT NULL,
	free INTEGER NOT NULL,
	respawn_time INTEGER,
	price INTEGER,
	sprite TEXT NOT NULL,
	CHECK(strength <= 100 AND strength >= 0),
	CHECK((free == 0 AND respawn_time != null) OR (free == 1 AND price != null))
);

CREATE TABLE pokemon_user (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	pokemon_id INTEGER REFERENCES pokemon(id),
	user_id INTEGER REFERENCES app_user(id),
	catch_date TEXT,
	nickname TEXT
);

CREATE TABLE pokeball_user (
	user_id INTEGER REFERENCES app_user(id),
	pokeball_id INTEGER REFERENCES pokeball(id),
	quantity INTEGER,
	PRIMARY KEY (user_id, pokeball_id)
);

CREATE TABLE location (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	latitude REAL NOT NULL,
	longitude REAL NOT NULL,
	radius REAL NOT NULL,
	pokemon_id INTEGER REFERENCES pokemon(id)
);

CREATE TABLE weather (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	identifier TEXT NOT NULL
);

CREATE TABLE time_of_day (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	start_time INTEGER NOT NULL,
	end_time INTEGER NOT NULL,
	CHECK(start_time >= 0 AND start_time <= 24),
	CHECK(end_time >= 0 AND end_time <= 24)
);

CREATE TABLE notification (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	user_id INTEGER REFERENCES app_user(id),
	header TEXT NOT NULL,
	message_body TEXT,
	notification_date TEXT,
	notification_sprite TEXT
);

INSERT INTO time_of_day(start_time, end_time) VALUES(7,12);
INSERT INTO time_of_day(start_time, end_time) VALUES(1,23);
INSERT INTO time_of_day(start_time, end_time) VALUES(19,7);

INSERT INTO weather(identifier) VALUES("Sunny");
INSERT INTO weather(identifier) VALUES("Rainy");
INSERT INTO weather(identifier) VALUES("Snowing");

INSERT INTO pokemon(name, description, strength, weather_preference, time_preference, sprite_normal, sprite_small) VALUES ("Raichu", "This POKéMON exudes a weak electrical charge from all over its body that makes it take on a slight glow in darkness. RAICHU searches for electricity by planting its tail in the ground.", 50, 1, 1, "raichu_n", "raichu_s");
INSERT INTO pokemon(name, description, strength, weather_preference, time_preference, sprite_normal, sprite_small) VALUES ("Charizard", "CHARIZARD flies around the sky in search of powerful opponents. It breathes fire of such great heat that it melts anything. However, it never turns its fiery breath on any opponent weaker than itself.", 80, 1, 1, "charizard_n", "charizard_s");
INSERT INTO pokemon(name, description, strength, weather_preference, time_preference, sprite_normal, sprite_small) VALUES ("Zubat", "ZUBAT avoids sunlight because exposure causes it to become unhealthy. During the daytime, it stays in caves or under the eaves of old houses, sleeping while hanging upside down.", 30, 1, 3, "zubat_n", "zubat_s");
INSERT INTO pokemon(name, description, strength, weather_preference, time_preference, sprite_normal, sprite_small) VALUES ("Blastoise", "BLASTOISE has water spouts that protrude from its shell. The water spouts are very accurate. They can shoot bullets of water with enough accuracy to strike empty cans from a distance of over 160 feet.", 80, 2, 2, "blastoise_n", "blastoise_s");
INSERT INTO pokemon(name, description, strength, weather_preference, time_preference, sprite_normal, sprite_small) VALUES ("Venusaur", "There is a large flower on VENUSAUR’s back. The flower is said to take on vivid colors if it gets plenty of nutrition and sunlight. The flower’s aroma soothes the emotions of people.", 80, 1, 2, "venussaur_n", "venussaur_s");
INSERT INTO pokemon(name, description, strength, weather_preference, time_preference, sprite_normal, sprite_small) VALUES ("Agumon", "Agumon is a Reptile Digimon. It is a short and stout Tyrannosaurus rex-like Digimon standing about 3 feet tall, and has amber orange skin and light green eyes.", 100, 3, 2, "agumon_n", "agumon_s");

INSERT INTO app_user(username, password) VALUES("user1", "password1");
INSERT INTO app_user(username, password) VALUES("user2", "password2");
INSERT INTO app_user(username, password) VALUES("user3", "password3");

INSERT INTO pokeball(name, description, strength, free, respawn_time, price, sprite) VALUES("pokeball", "regular pokeball", 30, 1, 24, 0, "pokeball");
INSERT INTO pokeball(name, description, strength, free, respawn_time, price, sprite) VALUES("ultraball", "stronger pokeball", 60, 0, 0, 2, "ultraball");
INSERT INTO pokeball(name, description, strength, free, respawn_time, price, sprite) VALUES("masterball", "ultimate pokeball", 100, 0, 0, 10, "masterball");

INSERT INTO pokemon_user(pokemon_id, user_id, catch_date, nickname) VALUES(1,1,"11-11-2014","Andy");
INSERT INTO pokemon_user(pokemon_id, user_id, catch_date, nickname) VALUES(2,1,"13-11-2014","T-rex");
INSERT INTO pokemon_user(pokemon_id, user_id, catch_date, nickname) VALUES(4,2,"09-11-2014","Donatello");
INSERT INTO pokemon_user(pokemon_id, user_id, catch_date, nickname) VALUES(5,3,"15-11-2014","aaa");

INSERT INTO pokeball_user(user_id,pokeball_id,quantity) VALUES(1,1,5);
INSERT INTO pokeball_user(user_id,pokeball_id,quantity) VALUES(2,1,2);
INSERT INTO pokeball_user(user_id,pokeball_id,quantity) VALUES(2,2,1);
INSERT INTO pokeball_user(user_id,pokeball_id,quantity) VALUES(3,1,1);
INSERT INTO pokeball_user(user_id,pokeball_id,quantity) VALUES(3,3,100);

INSERT INTO location(latitude,longitude,radius,pokemon_id) VALUES(40.608182,-8.657769,0.0005,2);
INSERT INTO location(latitude,longitude,radius,pokemon_id) VALUES(41.110830,-8.533909,0.0005,3);
INSERT INTO location(latitude,longitude,radius,pokemon_id) VALUES(41.165087,-8.605351,0.0005,4);
INSERT INTO location(latitude,longitude,radius,pokemon_id) VALUES(41.140373,-8.636755,0.0005,6);
INSERT INTO location(latitude,longitude,radius,pokemon_id) VALUES(41.177048,-8.594421,0.0005,4);
INSERT INTO location(latitude,longitude,radius,pokemon_id) VALUES(41.177655,-8.595752,0.0005,1);
INSERT INTO location(latitude,longitude,radius,pokemon_id) VALUES(41.178557,-8.597506,0.0005,5);

INSERT INTO notification(user_id, header, message_body, notification_date, notification_sprite) VALUES(1,"Welcome","Welcome to pokecaching, we hope you have fun. Gotta catch'em'all!", "11-11-2014","notification");
INSERT INTO notification(user_id, header, message_body, notification_date, notification_sprite) VALUES(2,"Welcome","Welcome to pokecaching, we hope you have fun. Gotta catch'em'all!", "11-11-2014","notification");
INSERT INTO notification(user_id, header, message_body, notification_date, notification_sprite) VALUES(3,"Welcome","Welcome to pokecaching, we hope you have fun. Gotta catch'em'all!", "11-11-2014","notification");
