var Notification = function(id, header, body, date, sprite) {
	this.id = id;
	this.header = header;
	this.body = body;
	this.date = date;
	this.sprite = sprite;
}

Notification.prototype.id = -1;
Notification.prototype.header = "";
Notification.prototype.body = "";
Notification.prototype.date = "";
Notification.prototype.sprite = "";

module.exports = Notification;