var Pokemon = function (id, name, description, strength, weather_preference, time_preference, sprite_small, sprite_normal) {
	this.id = id;
	this.name = name;
	this.description = description;
	this.strength = strength;
	this.weather_preference = weather_preference;
	this.time_preference = time_preference;
	this.sprite_normal = sprite_normal;
	this.sprite_small = sprite_small;
}

Pokemon.prototype.id = -1;
Pokemon.prototype.name = "";
Pokemon.prototype.description = "";
Pokemon.prototype.strength = -1;
Pokemon.prototype.weather_preference = "";
Pokemon.prototype.time_preference = null;
Pokemon.prototype.sprite_normal = "";
Pokemon.prototype.sprite_small = "";

module.exports = Pokemon;
