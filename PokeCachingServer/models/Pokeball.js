var Pokeball = function (id, name, description, strength, free, respawn_time, price, sprite) {
	this.id = id;
	this.name = name;
	this.description = description;
	this.strength = strength;
	this.free = free;
	this.respawn_time = respawn_time;
	this.price = price;
	this.sprite = sprite;
}

Pokeball.prototype.id = -1;
Pokeball.prototype.name = "";
Pokeball.prototype.description = "";
Pokeball.prototype.strength = -1;
Pokeball.prototype.free = false;
Pokeball.prototype.respawn_time = 0;
Pokeball.prototype.price = 0;
Pokeball.prototype.sprite = "";

module.exports = Pokeball;