var User = function(id, username, password, pokemons, items, notifications) {
	this.id = id;
	this.username = username;
	this.password = password;
	this.pokemons = pokemons;
	this.items = items;
	this.notifications = notifications;
	//this.loggedIn = false;
}

User.prototype.id = -1;
User.prototype.username = "";
User.prototype.password = "";
User.prototype.pokemons = [];
User.prototype.items = [];
User.prototype.notifications = [];

module.exports = User;