var UserPokemon = function(id, pokemon, nickname, catchDate) {
	this.id = id;
	this.pokemon = pokemon;
	this.nickname = nickname;
	this.catchDate = catchDate;
}

UserPokemon.prototype.id = -1;
UserPokemon.prototype.pokemon = null;
UserPokemon.prototype.nickname = "";
UserPokemon.prototype.catchDate = "";

module.exports = UserPokemon;