var Location = function (id, latitude, longitude, radius, pokemon) {
	this.id = id;
	this.latitude = latitude;
	this.longitude = longitude;
	this.radius = radius;
	this.pokemon = pokemon;
}

Location.prototype.id = -1;
Location.prototype.latitude = 0;
Location.prototype.longitude = 0;
Location.prototype.radius = 0;
Location.prototype.pokemon = null;

module.exports = Location;