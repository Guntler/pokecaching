var UserItem = function(item, quantity) {
	this.item = item;
	this.quantity = quantity;
}

UserItem.prototype.item = null;
UserItem.prototype.quantity = 0;

module.exports = UserItem;