//
//	DATABASE MANAGEMENT MODULE
//	Provides methods to load and update the database.
//


var fs = require("fs");
var file = "database.db";
var exists = fs.existsSync(file);

var sqlite3 = require("sqlite3").verbose();
var db = new sqlite3.Database(file);

var Pokemon = require("./models/Pokemon");
var Location = require("./models/Location");
var User = require("./models/User");
var UserPokemon = require("./models/UserPokemon");
var UserItem = require("./models/UserItem");
var Pokeball = require("./models/Pokeball");
var Notification = require("./models/Notification");

function LoadPokemon(locations, pokemons, callback) {
	db.serialize(function() {
		if(exists) {
			var times = [];
			
			db.each("SELECT * FROM time_of_day", function(err, row) {
				times.push(row);
			});
			
			db.each("SELECT * FROM pokemon", function(err, row) {
				for(var i = 0; i < times.length; i++) {
					if(times[i].id === row.time_preference) {
						pokemons.push(new Pokemon(row.id, row.name, row.description, row.strength, 
												row.weather_preference, times[i], row.sprite_small, row.sprite_normal));
					}
				}
			});
			
			db.each("SELECT * FROM location", function(err, row) {
				var pokemon = null;
				for(var i = 0; i < pokemons.length; i++) {
					if(pokemons[i].id === row.pokemon_id) {
						pokemon = pokemons[i];
					}
				}
				
				var location = null;
				if(pokemon != null) {
					location = new Location(row.id, row.latitude, row.longitude, row.radius, pokemon);
					locations.push(location);
				}
			},callback);
		}
	});
}

function LoadUsers(users, pokemons, items) {
	db.serialize(function() {
		if(exists) {
		
			var owned_pokemon = [];
			db.each("SELECT pokemon.*, pokemon_user.user_id, pokemon_user.catch_date, pokemon_user.nickname, pokemon_user.id AS companion_id FROM pokemon, pokemon_user WHERE pokemon_user.pokemon_id = pokemon.id", 
			function(err, row) {
				owned_pokemon.push(row);
			});
			
			owned_items = [];
			db.each("SELECT pokeball.*, pokeball_user.quantity, pokeball_user.user_id FROM pokeball, pokeball_user WHERE pokeball_user.pokeball_id = pokeball.id", 
			function(err, row) {
				owned_items.push(row);
			});
			
			notifications = [];
			db.each("SELECT * FROM notification", function(err, row) {
				notifications.push(row);
			});
			
			db.each("SELECT * FROM app_user", function(err, row) {
				var user_pokemons = [];
				
				for(var i = 0; i < owned_pokemon.length; i++) {
					if(owned_pokemon[i].user_id === row.id) {
						for(var j = 0; j < pokemons.length; j++) {
							if(pokemons[j].id === owned_pokemon[i].id) {
								var user_pokemon = new UserPokemon(owned_pokemon[i].companion_id, pokemons[j], owned_pokemon[i].nickname, owned_pokemon[i].catch_date);
								user_pokemons.push(user_pokemon);
								break;
							}
						}
					}
				}
				
				var user_items = [];
				
				for(var i = 0; i < owned_items.length; i++) {
					if(owned_items[i].user_id === row.id) {
						for(var j = 0; j < items.length; j++) {
							if(items[j].id === owned_items[i].id) {
								var item = new UserItem(items[j], owned_items[i].quantity);
								user_items.push(item);
							}
						}
					}
				}
				
				var user_notifications = [];
				
				for(var i = 0; i < notifications.length; i++) {
					if(notifications[i].user_id === row.id) {
						var notification = new Notification(notifications[i].id, notifications[i].header, notifications[i].message_body, 
																notifications[i].notification_date, notifications[i].notification_sprite);
						user_notifications.push(notification);
					}
				}
				
				var user = new User(row.id, row.username, row.password, user_pokemons, user_items, user_notifications);
				users.push(user);
			});
		}
	});
	
	for(var i = 0; i < items.length; i++) {
		if(items.free) {
			setInterval(function() {
				console.log("Giving item " + items[i] + " to users.");
				for(var j = 0; j < users.length; j++) {
					UpdateUserItems(users[j].username, items[i].id, 1, true, users, items, function(){});
				}
			},items.respawn_time*3600000);
		}
	}
			
}

function LoadItems(items, callback) {
	db.serialize(function() {
		if(exists) {
			db.each("SELECT * FROM pokeball", 
			function(err, row) {
				var isFree = false;
				if(row.free === 1)
					isFree = true;
				var pokeball = new Pokeball(row.id, row.name, row.description, row.strength, isFree, row.respawn_time, row.price, row.sprite);
				items.push(pokeball);
			}, callback);
		}
	});
}

//Loads database into memory.
exports.LoadDatabase = function(locations,pokemons,items,users) {
	var loadUsersFunc = function(err, numRows) {
		if(err != null)
			console.log("Error loading items");
		else {
			LoadUsers(users, pokemons, items);
		}
	};
	
	var loadItemsFunc = function(err, numRows) {
		if(err != null)
			console.log("Error loading pokemon");
		else
			LoadItems(items,loadUsersFunc);
	};
	
	LoadPokemon(locations, pokemons, loadItemsFunc);
}

//Registers a new user pokemon in the database.
exports.RegisterPokemon = function(username, newUserPokemonId, catch_date, nickname, users, pokemons, callback) {
	var user = null;
	for(var i = 0; i < users.length; i++) {
		if(users[i].username === username) {
			user = users[i];
			break;
		}
	}
	
	if(user != null) {
		db.serialize(function() {
			if(exists) {
				db.run("INSERT INTO pokemon_user(pokemon_id, user_id, catch_date, nickname) VALUES (?,?,?,?)", [newUserPokemonId, user.id, catch_date, nickname], function(error) {
					if(error != null)
						console.log(error);
					else {
						for(var i = 0; i < pokemons.length; i++) {
							if(pokemons[i].id == newUserPokemonId) {
								var pokemon = new UserPokemon(this.lastID, pokemons[i], nickname, catch_date);
								console.log(JSON.stringify(pokemon));
								console.log("Last id is " + this.lastID);
								user.pokemons.push(pokemon);
								callback(true);
								return;
							}
						}
					}
					
					callback(false);
					return;
				});
			} else callback(false);
		});
	} else callback(false);
}

//Unregisters the user pokemon with the specified Id.
exports.UnregisterPokemon = function(pokemonId, users, callback) {
	var found = false;
	for(var i = 0; i < users.length; i++) {
		for(var j = 0; j < users[i].pokemons.length; j++) {
			if(users[i].pokemons[j].id == pokemonId) {
				found = true;
				var pokemon = users[i].pokemons[j];
				db.serialize(function() {
					if(exists) {
						db.run("DELETE FROM pokemon_user WHERE ? = pokemon_user.id", [pokemonId], function(error) {
							if(error != null)
								console.log(error);
							else {
								users[i].pokemons.splice(users[i].pokemons.indexOf(pokemon), 1);
								callback(true);
								return;
							}
							
							callback(false);
						});
					}
				});
				return;
			}
		}
	}
	if(!found)
		callback(false);
}

//Registers a new account.
exports.RegisterUser = function(username, password, users, callback) {
	db.serialize(function() {
		if(exists) {
			db.run("INSERT INTO app_user(username,password) VALUES(?,?)", [username,password], function(error) {
				if(error != null)
					console.log(error);
				else {
					users.push(new User(this.lastId, username, password, [], [], []));
					callback(true);
					return;
				}
				
				callback(false);
			});
		} else callback(false);
	});
}

//Logs the user in.
exports.LoginUser = function(username, password, users, callback) {
	for(var i = 0; i < users.length; i++) {
		if(users[i].username === username && users[i].password == password) {
			callback(true);
			return;
		}
	}
	
	callback(false);
}

//Updates a user's items.
exports.UpdateUserItems = function(username, itemId, quantity, add, users, items, callback) {
	var user = null;
	
	for(var i = 0; i < users.length; i++) {
		if(users[i].username === username) {
			user = users[i];
			break;
		}
	}
	
	if(user != null) {
		db.serialize(function() {
			if(exists) {
				for(var i = 0; i < user.items.length; i++) {
					if(user.items[i].item.id == itemId) {
						if(add == "true") 
							user.items[i].quantity += quantity;
						else user.items[i].quantity -= quantity;
						
						if(user.items[i].quantity < 0) {
							db.run("DELETE FROM pokeball_user WHERE user_pokeball.id = ?", [itemId], function(error) {
								if(error != null)
									console.log(error);
								else {
									user.items.splice(user.items.indexOf(user.items[i]), 1);
									callback(true);
									return;
								}
								
								callback(false);
								return;
							});
						}
						else {
							
							db.run("UPDATE pokeball_user SET quantity = ? WHERE pokeball_user.pokeball_id = ?", [user.items[i].quantity,itemId], function(error) {
								if(error != null)
									console.log(error);
								else {
									callback(true);
									return;
								}
								
								callback(false);
								return;
							});
							
							return;
						}
					}
				}
				
				if(add == "true" && quantity > 0) {
					var found = false;
					for(var i = 0; i < items.length; i++) {
						if(items[i].id == itemId) {
							found = true;
							db.run("INSERT INTO pokeball_user(user_id, pokeball_id, quantity) VALUES(?,?,?)", [user.id, itemId, quantity], function(error) {
								if(error != null)
									console.log(error);
								else {
									user.items.push(new UserItem(items[i],quantity));
									callback(true);
									return;
								}
								
								callback(false);
								return;
							});
						}
						if(!found)
							callback(false);
					}
				}
			}
		});
	} else callback(false);
}

//Adds a notification to a user.
exports.AddNotification = function(username, header, body, sprite, users, callback) {
	var user = null;
	
	for(var i = 0; i < users.length; i++) {
		if(users[i].username === username) {
			user = users[i];
			break;
		}
	}
	
	if(user != null) {
		db.serialize(function() {
			if(exists) {
				var date = new Date();
 
				var yyyy = date.getFullYear().toString();
				var mm = (date.getMonth()+1).toString();
				var dd  = date.getDate().toString();
				
				var mmChars = mm.split('');
				var ddChars = dd.split('');
				
				var datestring = (ddChars[1]?dd:"0"+ddChars[0]) + '-' + (mmChars[1]?mm:"0"+mmChars[0]) + '-' + yyyy;
			
				db.run("INSERT INTO notification(user_id, header, message_body, notification_date, notification_sprite) VALUES(?,?,?,?,?)", [user.id, header,body,datestring,sprite], function(error) {
					if(error != null)
						console.log(error);
					else {
						user.notifications.push(new Notification(this.lastId,header,body,datestring,sprite));
						callback(true);
						return;
					}
					callback(false);
					return;
				});
			} else callback(false);
		});
	} else callback(false);
}

//Removes a notification with the specified Id.
exports.RemoveNotification = function(notificationId, users, callback) {
	var found = false;
	for(var i = 0; i < users.length; i++) {
		for(var j = 0; j < users[i].notifications.length; j++) {
			if(users[i].notifications[j].id === notificationId) {
				found = true;
				var notification = users[i].notifications[j];
				db.serialize(function() {
					if(exists) {
						db.run("DELETE FROM notification WHERE notification.id = ?", [notificationId], function(error) {
							if(error != null)
								console.log(error);
							else {
								users[i].notifications.splice(users[i].notifications.indexOf(notification), 1);
								callback(true);
								return;
							}
							
							callback(false);
						});
					}
				});
				return;
			}
		}
	}
	
	if(!found)
		callback(false);
}