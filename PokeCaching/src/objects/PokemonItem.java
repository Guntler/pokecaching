package objects;

public class PokemonItem {
    private int imageId;
    private String title;
    private String desc;
    private String weather;
    private String time;
    private String index;
     
    public PokemonItem(int imageId, String title, String desc, String weather, String time, String index) {
        this.imageId = imageId;
        this.title = title;
        this.desc = desc;
        this.index = index;
        this.weather = weather;
        this.time = time;
    }
    public int getImageId() {
        return imageId;
    }
    public void setImageId(int imageId) {
        this.imageId = imageId;
    }
    public String getIndex() {
        return index;
    }
    public void setIndex(String index) {
        this.index = index;
    }
    public String getDesc() {
        return desc;
    }
    public void setDesc(String desc) {
        this.desc = desc;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    @Override
    public String toString() {
        return title + "\n" + desc;
    }
	public String getWeather() {
		return weather;
	}
	public void setWeather(String weather) {
		this.weather = weather;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}   
}