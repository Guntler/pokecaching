package objects;

public class RowItem {
    private int imageId;
    private String title;
    private String desc;
    private String index;
     
    public RowItem(int imageId, String title, String string, String index) {
        this.imageId = imageId;
        this.title = title;
        this.desc = string;
        this.index = index;
    }
    public int getImageId() {
        return imageId;
    }
    public void setImageId(int imageId) {
        this.imageId = imageId;
    }
    public String getIndex() {
        return index;
    }
    public void setIndex(String index) {
        this.index = index;
    }
    public String getDesc() {
        return desc;
    }
    public void setDesc(String desc) {
        this.desc = desc;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    @Override
    public String toString() {
        return title + "\n" + desc;
    }   
}