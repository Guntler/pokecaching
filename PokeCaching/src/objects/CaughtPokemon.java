package objects;

public class CaughtPokemon {
	private int id;
	private Pokemon pokemon;
	private String nickname;
	private String date;
	
	public CaughtPokemon(int id, Pokemon pkmn, String nickname, String date) {
		this.setId(id);
		this.setPokemon(pkmn);
		this.setNickname(nickname);
		this.setDate(date);
	}

	public Pokemon getPokemon() {
		return pokemon;
	}

	public void setPokemon(Pokemon pokemon) {
		this.pokemon = pokemon;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
