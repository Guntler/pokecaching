package objects;

public class TimeRange {
	private int startTime;
	private int endTime;
	
	public TimeRange(int st, int et) {
		setStartTime(st);
		setEndTime(et);
	}

	public int getStartTime() {
		return startTime;
	}

	public void setStartTime(int startTime) {
		this.startTime = startTime;
	}

	public int getEndTime() {
		return endTime;
	}

	public void setEndTime(int endTime) {
		this.endTime = endTime;
	}
}
