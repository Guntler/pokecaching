package objects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class User extends BaseObject {
	private String username;
	private String password;
	private ArrayList<CaughtPokemon> caught;
	private Map<Pokeball,Integer> inventory;
	
	public User(String username, String password) {
		caught = new ArrayList<CaughtPokemon>();
		inventory = new HashMap<Pokeball,Integer>();
		this.setUsername(username);
		this.setPassword(password);
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public void addCaught(CaughtPokemon pkmn) {
		caught.add(pkmn);
	}
	
	public List<CaughtPokemon> getCaught() {
		return caught;
	}
	
	public void addPokeball(int amt, Pokeball ball) {
		if(inventory.containsKey(ball))
			inventory.put(ball, inventory.get(ball)+ amt);
		else
			inventory.put(ball, amt);
	}
	
	public Map<Pokeball,Integer> getInventory() {
		return inventory;
	}
}
