package objects;

public class Pokeball extends BaseObject {
	private int id;
	private String name;
	private int catchPower;
	private String description;
	private int respawnTime;
	private int price;
	private String sprite;
	private boolean free;

	public Pokeball(int id, String name, int catchRate, String dscrptn, int rTime, int prc, String sprite, boolean fr) {
		this.setId(id);
		this.setName(name);
		this.catchPower = catchRate;
		description = dscrptn;
		respawnTime = rTime;
		price = prc;
		this.setSprite(sprite);
		free = fr;
	}
	
	public float getCatchPower() {
		return catchPower;
	}

	public void setCatchPower(int catchRate) {
		this.catchPower = catchRate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getRespawnTime() {
		return respawnTime;
	}

	public void setRespawnTime(int respawnTime) {
		this.respawnTime = respawnTime;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public boolean isFree() {
		return free;
	}

	public void setFree(boolean free) {
		this.free = free;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSprite() {
		return sprite;
	}

	public void setSprite(String sprite) {
		this.sprite = sprite;
	}
	
}
