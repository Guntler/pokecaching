package objects;

public class Location {
	private float latitude;
	private float longitude;
	private float radius;
	
	public Location(float lat, float lon, float rad) {
		latitude = lat;
		longitude = lon;
		radius = rad;
	}
	
	public float getLatitude() {
		return latitude;
	}
	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}
	public float getLongitude() {
		return longitude;
	}
	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}
	public float getRadius() {
		return radius;
	}
	public void setRadius(float radius) {
		this.radius = radius;
	}
}
