package objects;

public class Pokemon extends BaseObject {
	private int index;
	private String name;
	private String description;
	private int catchRate; 
	private Location location;
	private Weather weather;
	private TimeRange time;
	private String smallSprite;
	private String largeSprite;
	private boolean loaded;
	
	public Pokemon(int idx, String nm, String dscptn, int cRate, Location loc, Weather wthr, TimeRange tm, String sSpr, String lSpr) {
		index = idx;
		name = nm;
		description = dscptn;
		catchRate = cRate;
		location = loc;
		weather = wthr;
		time = tm;
		smallSprite = sSpr;
		largeSprite = lSpr;
		setLoaded(false);
	}

	public Pokemon(Pokemon pkmn) {
		index = pkmn.getIndex();
		name = pkmn.getName();
		description = pkmn.getDescription();
		catchRate = pkmn.getCatchRate();
		location = pkmn.getLocation();
		weather = pkmn.getWeather();
		time = pkmn.getTime();
		smallSprite = pkmn.getSmallSprite();
		largeSprite = pkmn.getLargeSprite();
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public int getCatchRate() {
		return catchRate;
	}


	public void setCatchRate(int catchRate) {
		this.catchRate = catchRate;
	}


	public Location getLocation() {
		return location;
	}


	public void setLocation(Location location) {
		this.location = location;
	}


	public Weather getWeather() {
		return weather;
	}


	public void setWeather(Weather weather) {
		this.weather = weather;
	}


	public TimeRange getTime() {
		return time;
	}


	public void setTime(TimeRange time) {
		this.time = time;
	}


	public String getSmallSprite() {
		return smallSprite;
	}


	public void setSmallSprite(String smallSprite) {
		this.smallSprite = smallSprite;
	}


	public String getLargeSprite() {
		return largeSprite;
	}


	public void setLargeSprite(String largeSprite) {
		this.largeSprite = largeSprite;
	}

	public boolean isLoaded() {
		return loaded;
	}

	public void setLoaded(boolean loaded) {
		this.loaded = loaded;
	}
}
