package objects;

public class Notification {
	private String header;
	private String body;
	private TimeStamp date;
	private String sprite;
	
	public Notification(String header, String body, TimeStamp date, String sprite) {
		this.setHeader(header);
		this.setBody(body);
		this.setDate(date);
		this.setSprite(sprite);
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public TimeStamp getDate() {
		return date;
	}

	public void setDate(TimeStamp date) {
		this.date = date;
	}

	public String getSprite() {
		return sprite;
	}

	public void setSprite(String sprite) {
		this.sprite = sprite;
	}
}
