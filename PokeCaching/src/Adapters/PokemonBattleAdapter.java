package Adapters;


import java.util.List;

import com.example.pokecaching.PokemonHolder;
import com.example.pokecaching.R;

import objects.PokemonItem;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

// http://stackoverflow.com/questions/6783327/setimageresource-from-a-string
// https://devtut.wordpress.com/2011/06/09/custom-arrayadapter-for-a-listview-android/
public class PokemonBattleAdapter extends ArrayAdapter<PokemonItem> {
	private final Context context;
 
	public PokemonBattleAdapter(Context context, int resourceId, List<PokemonItem> items) {
		super(context, resourceId, items);
		this.context = context;
	}
	
	public static int getImageId(Context context, String imageName) {
	    return context.getResources().getIdentifier("drawable/" + imageName, null, context.getPackageName());
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		PokemonHolder holder = null;
		PokemonItem pkmnItem = getItem(position);
        
		LayoutInflater inflater = (LayoutInflater) context
			.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.list_battle_pkmn, parent, false);
			holder = new PokemonHolder();
			holder.imageView = (ImageView) convertView.findViewById(R.id.icon);
			holder.txtName = (TextView) convertView.findViewById(R.id.title);
			holder.txtDesc = (TextView) convertView.findViewById(R.id.desc);
			holder.txtIndex = (TextView) convertView.findViewById(R.id.index);
			convertView.setTag(holder);
		}
		else
			holder = (PokemonHolder) convertView.getTag();
		
		holder.txtName.setText(pkmnItem.getTitle());
		holder.txtDesc.setText(pkmnItem.getDesc());
		holder.txtIndex.setText(pkmnItem.getIndex());
        holder.imageView.setImageResource(pkmnItem.getImageId());
 
		return convertView;
	}
}
