package Adapters;


import java.util.List;

import com.example.pokecaching.R;

import objects.RowItem;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

// http://stackoverflow.com/questions/6783327/setimageresource-from-a-string
// https://devtut.wordpress.com/2011/06/09/custom-arrayadapter-for-a-listview-android/
public class RowItemArrayAdapter extends ArrayAdapter<RowItem> {
	private final Context context;
 
	public RowItemArrayAdapter(Context context, int resourceId, List<RowItem> items) {
		super(context, resourceId, items);
		this.context = context;
	}
	
	private class ViewHolder {
        ImageView imageView;
        TextView txtTitle;
        TextView txtDesc;
        TextView txtIdx;
    }
	
	public static int getImageId(Context context, String imageName) {
	    return context.getResources().getIdentifier("drawable/" + imageName, null, context.getPackageName());
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		RowItem rowItem = getItem(position);
        
		LayoutInflater inflater = (LayoutInflater) context
			.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.list_pokedex, parent, false);
			holder = new ViewHolder();
			holder.imageView = (ImageView) convertView.findViewById(R.id.icon);
			holder.txtTitle = (TextView) convertView.findViewById(R.id.title);
			holder.txtDesc = (TextView) convertView.findViewById(R.id.desc);
			holder.txtIdx = (TextView) convertView.findViewById(R.id.index);
			convertView.setTag(holder);
		}
		else
			holder = (ViewHolder) convertView.getTag();
		
		holder.txtTitle.setText(rowItem.getTitle());
		holder.txtDesc.setText(rowItem.getDesc());
		holder.txtIdx.setText(rowItem.getIndex());
        holder.imageView.setImageResource(rowItem.getImageId());
		//holder.txtTitle.setText(pkmn.getName());
		//holder.imageView.setImageResource(getImageId(context, pkmn.getSmallSprite()));
 
		return convertView;
	}
}
