package packets;

import argo.jdom.JsonObjectNodeBuilder;
import objects.CaughtPokemon;

import static argo.jdom.JsonNodeBuilders.*;

public class StorePokemonPacket extends Packet {
	public StorePokemonPacket(String us, CaughtPokemon pkmn) {
		JsonObjectNodeBuilder builder = anObjectBuilder()
				.withField("type", aStringBuilder("STORE_POKEMON"))
				.withField("username", aStringBuilder(us))
				.withField("id", aNumberBuilder(Integer.toString(pkmn.getPokemon().getIndex())))
				.withField("nickname", aStringBuilder(pkmn.getNickname()))
				.withField("date", aStringBuilder(pkmn.getDate()));
		
		json = builder.build();
	}
}
