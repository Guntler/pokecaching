package packets;

import static argo.jdom.JsonNodeFactories.field;
import static argo.jdom.JsonNodeFactories.object;
import static argo.jdom.JsonNodeFactories.string;

public class RegisterPacket extends Packet {
	public RegisterPacket(String us, String pass) {
		json = object(
				field("type", string("REGISTER")),
				field("username", string(us)),
				field("password", string(pass))
				);
	}
}
