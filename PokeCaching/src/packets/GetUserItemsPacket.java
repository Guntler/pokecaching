package packets;

import static argo.jdom.JsonNodeFactories.field;
import static argo.jdom.JsonNodeFactories.object;
import static argo.jdom.JsonNodeFactories.string;

public class GetUserItemsPacket extends Packet {
	public GetUserItemsPacket(String us) {
		json = object(
				field("type", string("GET_USER_ITEMS")),
				field("username", string(us))
				);
	}
}
