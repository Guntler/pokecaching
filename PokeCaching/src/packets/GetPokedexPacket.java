package packets;

import static argo.jdom.JsonNodeFactories.field;
import static argo.jdom.JsonNodeFactories.object;
import static argo.jdom.JsonNodeFactories.string;

public class GetPokedexPacket extends Packet {
	public GetPokedexPacket(String us) {
		json = object(
				field("type", string("GET_POKEDEX")),
				field("username", string(us))
				);
	}
}
