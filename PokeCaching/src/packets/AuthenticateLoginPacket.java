package packets;

import static argo.jdom.JsonNodeFactories.*;
import argo.jdom.JsonNodeBuilder;
import argo.jdom.JsonRootNode;

public class AuthenticateLoginPacket extends Packet {
	public AuthenticateLoginPacket(String us, String pass) {
		json = object(
				field("type", string("AUTHENTICATE_LOGIN")),
				field("username", string(us)),
				field("password", string(pass))
				);
	}
}
