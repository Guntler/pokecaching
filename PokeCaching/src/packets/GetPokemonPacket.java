package packets;

import static argo.jdom.JsonNodeFactories.field;
import static argo.jdom.JsonNodeFactories.object;
import static argo.jdom.JsonNodeFactories.string;
import static argo.jdom.JsonNodeFactories.number;

public class GetPokemonPacket extends Packet {
	public GetPokemonPacket(int id) {
		json = object(
				field("type", string("GET_POKEMON")),
				field("id", number(id))
				);
	}
}
