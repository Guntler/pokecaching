package packets;

import static argo.jdom.JsonNodeFactories.field;
import static argo.jdom.JsonNodeFactories.object;
import static argo.jdom.JsonNodeFactories.string;
import static argo.jdom.JsonNodeFactories.number;

public class RemovePokemonPacket extends Packet {
	public RemovePokemonPacket(String us, int id) {
		json = object(
				field("type", string("REMOVE_POKEMON")),
				field("username", string(us)),
				field("id", number(id))
				);
	}
}
