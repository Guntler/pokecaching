package packets;

import static argo.jdom.JsonNodeFactories.field;
import static argo.jdom.JsonNodeFactories.number;
import static argo.jdom.JsonNodeFactories.object;
import static argo.jdom.JsonNodeFactories.string;

public class GetPartyPokemonInfoPacket extends Packet {
	public GetPartyPokemonInfoPacket(String us) {
		json = object(
				field("type", string("GET_PARTY_POKEMON_INFO")),
				field("username", string(us))
				);
	}
}
