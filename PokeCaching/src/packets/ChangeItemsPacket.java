package packets;

import static argo.jdom.JsonNodeFactories.*;

public class ChangeItemsPacket extends Packet {
	public ChangeItemsPacket(boolean add, String us, int id, int quantity) {
		String bool;
		if(add)
			bool = "true";
		else
			bool = "false";
		json = object(
				field("type", string("CHANGE_ITEMS")),
				field("add", string(bool)),
				field("username", string(us)),
				field("id", number(Integer.toString(id))),
				field("quantity", number(Integer.toString(quantity)))
				);
	}
}
