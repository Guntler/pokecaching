package packets;

import argo.jdom.JsonRootNode;

public class Packet {
	protected JsonRootNode json;

	public JsonRootNode getJson() {
		return json;
	}

	public void setJson(JsonRootNode json) {
		this.json = json;
	} 
}
