package packets;

import static argo.jdom.JsonNodeFactories.field;
import static argo.jdom.JsonNodeFactories.object;
import static argo.jdom.JsonNodeFactories.string;

public class GetItemsPacket extends Packet {
	public GetItemsPacket() {
		json = object(
				field("type", string("GET_ITEMS"))
				);
	}
}
