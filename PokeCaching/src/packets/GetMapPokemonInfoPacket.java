package packets;

import static argo.jdom.JsonNodeFactories.field;
import static argo.jdom.JsonNodeFactories.object;
import static argo.jdom.JsonNodeFactories.string;
import static argo.jdom.JsonNodeFactories.number;

import java.math.BigDecimal;

public class GetMapPokemonInfoPacket extends Packet {
	public GetMapPokemonInfoPacket (double d, double e) {
		json = object(
				field("type", string("GET_MAP_POKEMON_INFO")),
				field("latitude", number(new BigDecimal(d))),
				field("longitude", number(new BigDecimal(e)))
				);
	}
}
