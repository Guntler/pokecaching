package handlers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.location.Location;
import android.os.AsyncTask;
import argo.jdom.JdomParser;
import argo.jdom.JsonNode;
import argo.jdom.JsonRootNode;
import argo.saj.InvalidSyntaxException;

import com.example.pokecaching.UpdateItemsTask;
import com.example.pokecaching.UpdateLocationTask;

import objects.CaughtPokemon;
import objects.Pokeball;
import objects.Pokemon;
import objects.User;
import objects.Weather;

public class PokecachingClient {
	public static boolean commandResponse;
	public static ArrayList<Pokemon> areaPkmn;
	public static ArrayList<Pokemon> pokedex;
	public static ArrayList<Pokemon> ramPkmn;
	public static ArrayList<Pokeball> items;
	public static User user = null;
	public static UpdateLocationTask lTask = null;
	public static UpdateItemsTask iTask = null;
	
	public static int currHour;
	public static Weather currWeather;
	public static Location curLocation;
	public static int prevMinute = 0;
	public static int prevHour = 0;
	public static final String PREFS_NAME = "MyPrefsFile";
	public static final String PREF_USERNAME = "username";
	public static final String PREF_PASSWORD = "password";
	
	public static void init() {
		curLocation= null;
		pokedex = new ArrayList<Pokemon>();
		areaPkmn = new ArrayList<Pokemon>();
		ramPkmn = new ArrayList<Pokemon>();
		items = new ArrayList<Pokeball>();
		commandResponse = false;
		Calendar c = Calendar.getInstance();
		int hours = c.get(Calendar.HOUR_OF_DAY);
		currHour = hours;
		prevMinute = c.get(Calendar.MINUTE);
		prevMinute = hours;
		currWeather = Weather.Sunny;
	}
	
	public static void addPokemonToArea(Pokemon pkmn) {
		areaPkmn.add(pkmn);
	}
	
	public static void addPokemonToPokedex(Pokemon pkmn) {
		pokedex.add(pkmn);
	}
	
	public static void addPokemonToMemory(Pokemon pkmn) {
		for(int i=0; i<pokedex.size(); i++) {
			if(pokedex.get(i).getIndex() == pkmn.getIndex()) {
				pkmn.setLoaded(true);
				pokedex.set(i, pkmn);
			}
		}
	}
	
	public static void removePokemonFromMemory() {
		ramPkmn.clear();
	}
	
	public static void storeResponse(boolean response) {
		commandResponse = response;
	}
	
	public static void addItemToUser(Pokeball ball, int amt) {
		user.addPokeball(amt, ball);
	}
	
	public static void addItem(Pokeball ball) {
		items.add(ball);
	}
	
	public static void addCaughtPokemon(CaughtPokemon pkmn) {
		user.addCaught(pkmn);
	}
	
	public static boolean retrieveCommandResponse() {
		boolean response = commandResponse;
		commandResponse=false;
		return response;
	}
}
