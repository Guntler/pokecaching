package handlers;

import java.util.List;

import objects.CaughtPokemon;
import objects.Location;
import objects.Pokeball;
import objects.Pokemon;
import objects.TimeRange;
import objects.Weather;
import argo.jdom.JdomParser;
import argo.jdom.JsonNode;
import argo.jdom.JsonRootNode;
import argo.saj.InvalidSyntaxException;

public class PacketHandler {
	
	static JdomParser parser = new JdomParser();
	static JsonRootNode packetBody;
	/*
	 	<status-line>
		<general-headers>
		<response-headers>
		<entity-headers>
		<empty-line>
		[<message-body>]
		[<message-trailers>]
	 */
	
	public static void parse(String msg) {
		try {
			packetBody = parser.parse(msg);
		} catch (InvalidSyntaxException e) {e.printStackTrace();}
		
		if(packetBody.getStringValue("type").equals("COMMAND_RESPONSE")) {
			parseCommandResponse();
		}
		else if(packetBody.getStringValue("type").equals("SEND_MAP_POKEMON")) {
			parseMapPokemon();
		}
		else if(packetBody.getStringValue("type").equals("SEND_POKEDEX")) {
			parsePokedex();
		}
		else if(packetBody.getStringValue("type").equals("SEND_POKEMON")) {
			parsePokemon();
		}
		else if(packetBody.getStringValue("type").equals("SEND_ITEMS")) {
			parseItems();
		}
		else if(packetBody.getStringValue("type").equals("SEND_PARTY_POKEMON")) {
			parsePartyPokemon();
		}
		else if(packetBody.getStringValue("type").equals("SEND_USER_ITEMS")) {
			parseUserItems();
		}
		else if(packetBody.getStringValue("type").equals("SEND_NOTIFICATION")) {
			int id = Integer.parseInt(packetBody.getStringValue("id"));
			String header = packetBody.getStringValue("header");
			String body = packetBody.getStringValue("body");
			String sprite = packetBody.getStringValue("sprite");
			String date = packetBody.getStringValue("date");
			//TODO create pokemon using info
		}
	}

	/**
	 * 
	 */
	private static void parsePartyPokemon() {
		PokecachingClient.user.getCaught().clear();
		List<JsonNode> nodes = packetBody.getArrayNode("pokemons");
		for(int i=0; i<nodes.size(); i++) {
			int id = Integer.parseInt(nodes.get(i).getNumberValue("pokemonId"));
			int pokemonId = Integer.parseInt(nodes.get(i).getNumberValue("id"));
			String nickname = nodes.get(i).getStringValue("nickname");
			String dateCaught = nodes.get(i).getStringValue("dateCaught");
			Pokemon pkmn = null;
			for(Pokemon pkmnIdx : PokecachingClient.pokedex) {
				if(pkmnIdx.getIndex() == id) {
					pkmn = pkmnIdx;
					PokecachingClient.addCaughtPokemon(new CaughtPokemon(pokemonId, pkmn, nickname, dateCaught));
					break;
				}
			}
		}
	}

	/**
	 * 
	 */
	private static void parseUserItems() {
		List<JsonNode> nodes = packetBody.getArrayNode("items");
		for(int i=0; i<nodes.size(); i++) {
			int id = Integer.parseInt(nodes.get(i).getNumberValue("id"));
			int quantity = Integer.parseInt(nodes.get(i).getNumberValue("quantity"));
			for(Pokeball ball : PokecachingClient.items) {
				if(ball.getId() == id) {
					PokecachingClient.addItemToUser(ball, quantity);
					break;
				}
			}
		}
	}

	/**
	 * 
	 */
	private static void parseItems() {
		List<JsonNode> nodes = packetBody.getArrayNode("items");
		for(int i=0; i<nodes.size(); i++) {
			int id = Integer.parseInt(nodes.get(i).getNumberValue("id"));
			String name = nodes.get(i).getStringValue("name");
			String sprite = nodes.get(i).getStringValue("sprite");
			int cRate = Integer.parseInt(nodes.get(i).getNumberValue("strength"));
			String description = nodes.get(i).getStringValue("description");
			String free = nodes.get(i).getStringValue("free");
			boolean isFree;
			if(free.equals("true"))
				isFree = true;
			else
				isFree = false;
			int respawnTime = Integer.parseInt(nodes.get(i).getNumberValue("respawnTime"));
			int price = Integer.parseInt(nodes.get(i).getNumberValue("price"));
			PokecachingClient.addItem(new Pokeball(id, name, cRate, description,
													respawnTime, price, sprite, isFree));
		}
	}

	/**
	 * 
	 */
	private static void parsePokemon() {
		int id = Integer.parseInt(packetBody.getNumberValue("id"));
		String name = packetBody.getStringValue("name");
		String description = packetBody.getStringValue("description");
		String weather = packetBody.getNumberValue("weather");
		   Weather wth;
		   if(weather.equals("1"))
		    wth = Weather.Sunny;
		   else if (weather.equals("2"))
		    wth = Weather.Rainy;
		   else
		    wth = Weather.Snowy;
		  int timeStart = Integer.parseInt(packetBody.getNumberValue("timeStart"));
		   int timeEnd = Integer.parseInt(packetBody.getNumberValue("timeEnd"));
		String spriteNormal = packetBody.getStringValue("spriteNormal");
		String spriteSmall = packetBody.getStringValue("spriteSmall");
		
		PokecachingClient.addPokemonToMemory(new Pokemon(id, name, description, 0, new Location(0,0,0), wth,
														new TimeRange(timeStart, timeEnd), spriteSmall, spriteNormal));
	}

	/**
	 * 
	 */
	private static void parsePokedex() {
		PokecachingClient.pokedex.clear();
		List<JsonNode> nodes = packetBody.getArrayNode("pokemons");
		for(int i=0; i<nodes.size(); i++) {
			int id = Integer.parseInt(nodes.get(i).getNumberValue("id"));
			String name = nodes.get(i).getStringValue("name");
			String sprite = nodes.get(i).getStringValue("sprite");
			String weather = nodes.get(i).getNumberValue("weather");
			Weather wth;
			if(weather.equals("1"))
				wth = Weather.Sunny;
			else if (weather.equals("2"))
				wth = Weather.Rainy;
			else
				wth = Weather.Snowy;
			
			int timeStart = Integer.parseInt(nodes.get(i).getNumberValue("timeStart"));
			int timeEnd = Integer.parseInt(nodes.get(i).getNumberValue("timeEnd"));
			
			PokecachingClient.addPokemonToPokedex(new Pokemon(id, name, "", 0, new Location(0,0,0), wth,
					new TimeRange(timeStart, timeEnd), sprite, sprite));
		}
	}

	/**
	 * 
	 */
	private static void parseCommandResponse() {
		String success = packetBody.getStringValue("success");
		if(success.equals("yes")) {
			PokecachingClient.storeResponse(true);
		}
		else
			PokecachingClient.storeResponse(false);
	}

	/**
	 * 
	 */
	private static void parseMapPokemon() {
		PokecachingClient.areaPkmn.clear();
		List<JsonNode> nodes = packetBody.getArrayNode("pokemons");
		for(int i=0; i<nodes.size(); i++) {
			int id = Integer.parseInt(nodes.get(i).getNumberValue("id"));
			float lat = Float.parseFloat(nodes.get(i).getNumberValue("latitude"));
			float lon = Float.parseFloat(nodes.get(i).getNumberValue("longitude"));
			float radius = Float.parseFloat(nodes.get(i).getNumberValue("radius"));
			String weather = nodes.get(i).getNumberValue("weather");
			
			Weather wth;
			if(weather.equals("1"))
				wth = Weather.Sunny;
			else if (weather.equals("2"))
				wth = Weather.Rainy;
			else
				wth = Weather.Snowy;
			
			int timeStart = Integer.parseInt(nodes.get(i).getNumberValue("timeStart"));
			int timeEnd = Integer.parseInt(nodes.get(i).getNumberValue("timeEnd"));
			String sprite = nodes.get(i).getStringValue("sprite");
			Pokemon pkmn = null;
			for(Pokemon pkmnIdx : PokecachingClient.pokedex) {
				if(pkmnIdx.getIndex() == id) {
					pkmn = pkmnIdx;
					break;
				}
			}
			
			if(pkmn != null)
				PokecachingClient.addPokemonToArea(new Pokemon(id, pkmn.getName(), "", 0, new Location(lat,lon,radius), wth,
					new TimeRange(timeStart, timeEnd), sprite, sprite));
		}
	}
}

