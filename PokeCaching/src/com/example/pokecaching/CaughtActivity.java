
package com.example.pokecaching;

import handlers.PacketHandler;
import handlers.PokecachingClient;

import java.util.ArrayList;
import java.util.List;

import objects.CaughtPokemon;
import objects.Pokemon;
import objects.PokemonItem;
import packets.ChangeItemsPacket;
import packets.GetPartyPokemonInfoPacket;
import packets.StorePokemonPacket;
import Adapters.PokemonBattleAdapter;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.Time;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

//Disable returning to previous screen
public class CaughtActivity extends Activity {
	String name;
	static ListView listPkmn;
	EditText nickField;
	Pokemon caughtSpecies = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_caught);
		
		boolean found = false;
		Bundle b = getIntent().getExtras();
		name = b.getString("name");

		/*
		 * Load Pokemon into battle screen
		 */
		for(Pokemon pkmnIdx : PokecachingClient.pokedex)
			if(pkmnIdx.getName().equals(name)) {
				caughtSpecies = pkmnIdx;
				found = true;
				break;
			}
		
		if(!found)
			return;
		
		Toast toast = Toast.makeText(this, caughtSpecies.getName(), Toast.LENGTH_LONG);
		toast.show();
		
		nickField = (EditText) findViewById(R.id.nickname);
		nickField.setText(caughtSpecies.getName());
	}
	
	public void doSubmit(View view) {
		String nickname = nickField.getText().toString();
		
		Time time = new Time();
		time.setToNow();
		int year = time.year;
		int month = time.month +1;
		int day = time.monthDay;
		String date = Integer.toString(year) + "/" + Integer.toString(month) + "/" + Integer.toString(day);
		
		CaughtPokemon cPkmn = new CaughtPokemon(0, caughtSpecies, nickname, date);
		System.out.println("Preparing to send StorePokemonPacket for user " + PokecachingClient.user.getUsername()
							+ " and pokemon " + cPkmn.getNickname());
		String response;
		try {
			response = (new CommunicationTask().execute(new StorePokemonPacket(PokecachingClient.user.getUsername(), cPkmn), CommunicationTask.MessageType.PUT,Long.valueOf(1000))).get();
			if(response != null) PacketHandler.parse(response);
		} catch (Exception e) {
			Toast toast = Toast.makeText(view.getContext(), "There has been an unexpected error: " + e.toString(), Toast.LENGTH_LONG);
			toast.show();
		}
		if(PokecachingClient.retrieveCommandResponse()) {
			try {
				response = (new CommunicationTask().execute(new GetPartyPokemonInfoPacket(PokecachingClient.user.getUsername()), CommunicationTask.MessageType.GET,Long.valueOf(1000))).get();
				if(response != null) PacketHandler.parse(response);
				
				Intent intent = new Intent(this, MapActivity.class);
				startActivity(intent);
			} catch (Exception e) {
				Toast toast = Toast.makeText(view.getContext(), "There has been an unexpected error: " + e.toString(), Toast.LENGTH_LONG);
				toast.show();
			}
		}
	}
}
