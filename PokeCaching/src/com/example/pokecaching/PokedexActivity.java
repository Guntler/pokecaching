package com.example.pokecaching;

import handlers.PokecachingClient;

import java.util.ArrayList;
import java.util.List;

import objects.Pokemon;
import objects.RowItem;
import Adapters.RowItemArrayAdapter;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

// http://www.mkyong.com/android/android-listview-example/
public class PokedexActivity extends Activity implements OnItemClickListener {
	static ListView listView;
	List<RowItem> rowItems;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_pokedex);
		rowItems = new ArrayList<RowItem>();
		for(Pokemon pkmnIdx : PokecachingClient.pokedex) {
			RowItem item = new RowItem(	getImageId(this,pkmnIdx.getSmallSprite()),
														pkmnIdx.getName(),
														"#"+Integer.toString(pkmnIdx.getIndex()),
														""
														);
			rowItems.add(item);
		}
		
		listView = (ListView) findViewById(R.id.dexGrid);
		RowItemArrayAdapter adapter = new RowItemArrayAdapter(this,
	            R.layout.list_pokedex, rowItems);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(this);
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		Intent intent = new Intent(getApplicationContext(), PokemonActivity.class);

        intent.putExtra("id", position);
        startActivity(intent);
	}
	
	private static int getImageId(Context context, String imageName) {
	    return context.getResources().getIdentifier("drawable/" + imageName, null, context.getPackageName());
	}
}
