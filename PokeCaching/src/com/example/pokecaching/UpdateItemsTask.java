package com.example.pokecaching;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.util.Calendar;
import java.util.List;

import objects.CaughtPokemon;
import objects.Pokemon;
import objects.Weather;

import org.bitpipeline.lib.owm.OwmClient;
import org.bitpipeline.lib.owm.WeatherData;
import org.bitpipeline.lib.owm.WeatherStatusResponse;
import org.bitpipeline.lib.owm.WeatherData.WeatherCondition;
import org.bitpipeline.lib.owm.WeatherData.WeatherCondition.ConditionCode;
import org.json.JSONException;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import handlers.PacketHandler;
import handlers.PokecachingClient;
import packets.GetMapPokemonInfoPacket;
import packets.GetUserItemsPacket;
import packets.Packet;
import android.location.Location;
import android.os.AsyncTask;
import android.text.format.Time;
import argo.format.JsonFormatter;
import argo.format.PrettyJsonFormatter;
import argo.jdom.JdomParser;
import argo.jdom.JsonNode;
import argo.jdom.JsonRootNode;
import argo.saj.InvalidSyntaxException;

public class UpdateItemsTask extends AsyncTask<Void, Void, Void> {
	
    
	JsonFormatter JSON_FORMATTER = new PrettyJsonFormatter();
	private OwmClient weatherClient;
    @Override
    protected void onPreExecute() {
    }

  
    @Override
    protected void onProgressUpdate(Void ... values) {
    }

	@Override
	protected Void doInBackground(Void... params) {
		boolean running = true;
		
		weatherClient = new OwmClient();
		
    	while(running) {
    		try {
				if(PokecachingClient.user != null) {
			        String username = PokecachingClient.user.getUsername();
			        if(username != null) {
			        	String response;
			        	try {
			        		Packet p = new GetUserItemsPacket(username);
			        		Socket communicationSocket = new Socket("94.132.186.108", 5000);
			        		if(communicationSocket != null) {
			        			communicationSocket.setSoTimeout(1000);
			        			DataOutputStream outToServer = new DataOutputStream(communicationSocket.getOutputStream());
			        			BufferedReader inFromServer = new BufferedReader(new InputStreamReader(communicationSocket.getInputStream()));
			        			
			        			String body = JSON_FORMATTER.format(p.getJson());
			        			
			        			String sentence = "GET /whatever HTTP/1.1\r\nContent-Length: " + body.length() + "\r\n\r\n" + body;
			        			outToServer.writeBytes(sentence);
			        			
			        			char[] buffer = new char[4096];
			        			int result = inFromServer.read(buffer);
			        			
			        			if(result <= 0) {
			        				communicationSocket.close();
			        				return null;
			        			}
			        			
			    				response = new String(buffer);
			    				
			    				String[] responseTokens = response.split("\\r\\n\\r\\n");
			    				
			    				if(responseTokens.length >= 2) {
			    					String responseBody = responseTokens[1].trim();
			    					PacketHandler.parse(responseBody);
			    				}
			        			
			    				communicationSocket.close();
			        		}
			    			
			    		} catch (Exception e) {
			    			System.out.println("There has been an unexpected error while performing this task: " + e.toString());
			    			e.printStackTrace();
			    		}
			        	
			        }
			        
			        Location location = PokecachingClient.curLocation;
			        
				}
				Thread.sleep(300000);
			} catch (Exception e) {
				e.printStackTrace();
			}
    	}
		return null; //This runs on a different thread
	}
}

