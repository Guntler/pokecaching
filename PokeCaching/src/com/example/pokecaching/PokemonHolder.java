package com.example.pokecaching;

import android.widget.ImageView;
import android.widget.TextView;

public class PokemonHolder {
	public TextView txtName;
	public TextView txtDesc;
	public TextView txtTime;
	public TextView txtWeather;
	public TextView txtIndex;
	public ImageView imageView;
}
