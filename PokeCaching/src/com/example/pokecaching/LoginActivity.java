package com.example.pokecaching;

import handlers.PacketHandler;
import handlers.PokecachingClient;
import objects.User;
import packets.AuthenticateLoginPacket;
import packets.GetItemsPacket;
import packets.GetPartyPokemonInfoPacket;
import packets.GetPokedexPacket;
import packets.GetUserItemsPacket;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_login);
		
		SharedPreferences pref = getSharedPreferences(PokecachingClient.PREFS_NAME,MODE_PRIVATE);   
		String username = pref.getString(PokecachingClient.PREF_USERNAME, null);
		String password = pref.getString(PokecachingClient.PREF_PASSWORD, null);
		
		if (username != null && password != null) {
			EditText usernameView = (EditText) findViewById(R.id.username);
			EditText passwordView = (EditText) findViewById(R.id.password);
			
			usernameView.setText(username);
			passwordView.setText(password);
		}
	}

	/**
	 * 
	 * @param view
	 */
	public void doLogin(View view) {
		try {
			System.out.println("wthell");
			String user = ((EditText) findViewById(R.id.username)).getText().toString();
			String pass = ((EditText) findViewById(R.id.password)).getText().toString();
			System.out.println("sigh");
			String response = (new CommunicationTask().execute(new AuthenticateLoginPacket(user,pass), CommunicationTask.MessageType.GET,Long.valueOf(1000))).get();
			System.out.println("Returned. Got: " + response);
			//String response = MainActivity.cTask.sendMessage(CommunicationsManager.MessageType.GET, new AuthenticateLoginPacket(user,pass), 1000);
			//String response = MainActivity.cTask.getNextResponse(1000);
			System.out.println("wtf");
			
			if(response != null) {
				System.out.println("Response isnt null");
				PacketHandler.parse(response);
				if(PokecachingClient.commandResponse) {
					PokecachingClient.user = new User(user,pass);
					getSharedPreferences(PokecachingClient.PREFS_NAME,MODE_PRIVATE)
			        .edit()
			        .putString(PokecachingClient.PREF_USERNAME, user)
			        .putString(PokecachingClient.PREF_PASSWORD, pass)
			        .commit();
					response = (new CommunicationTask().execute(new GetPokedexPacket(user), CommunicationTask.MessageType.GET, Long.valueOf(1000))).get();
					//response = MainActivity.cTask.sendMessage(CommunicationsManager.MessageType.GET, new GetPokedexPacket(user), 1000);
					//response = MainActivity.cTask.getNextResponse(1000);
					
					if(response != null) {
						PacketHandler.parse(response);
						response = (new CommunicationTask().execute(new GetItemsPacket(), CommunicationTask.MessageType.GET, Long.valueOf(1000))).get();
						//response = MainActivity.cTask.sendMessage(CommunicationsManager.MessageType.GET, new GetItemsPacket(), 1000);
						//response = MainActivity.cTask.getNextResponse(1000);
						
						if(response != null) {
							PacketHandler.parse(response);
							response = (new CommunicationTask().execute(new GetPartyPokemonInfoPacket(user), CommunicationTask.MessageType.GET, Long.valueOf(1000))).get();
							//response = MainActivity.cTask.sendMessage(CommunicationsManager.MessageType.GET, new GetPartyPokemonInfoPacket(user), 1000);
							//response = MainActivity.cTask.getNextResponse(1000);
							
							if(response != null) {
								PacketHandler.parse(response);
								response = (new CommunicationTask().execute(new GetUserItemsPacket(user), CommunicationTask.MessageType.GET, Long.valueOf(1000))).get();
								//response = MainActivity.cTask.sendMessage(CommunicationsManager.MessageType.GET, new GetUserItemsPacket(user), 1000);
								//response = MainActivity.cTask.getNextResponse(1000);
								
								if(response != null) {
									PacketHandler.parse(response);

									Intent intent = new Intent(this, MapActivity.class);
									startActivity(intent);
								}
								else {
									Toast toast = Toast.makeText(view.getContext(), "Could not log into the server. It may be down or experiencing problems.", 10);
									toast.show();
								}
							}
						}
					}
				}
				else {
					Toast toast = Toast.makeText(view.getContext(), "Incorrect username or password.", 10);
					toast.show();
				}
			}
			else {
				Toast toast = Toast.makeText(view.getContext(), "Unable to connect to server. It may be down or experiencing problems.", 10);
				toast.show();
			}
		} catch (Exception e) {
			System.out.println("There has been an unexpected error: " + e.toString());
		}
	}
	
	public void startRegister(View view) {
		Intent intent = new Intent(this, RegisterActivity.class);
		
		startActivity(intent);
	}
}
