package com.example.pokecaching;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.Socket;

import objects.Weather;

import org.bitpipeline.lib.owm.OwmClient;
import org.bitpipeline.lib.owm.WeatherData;
import org.bitpipeline.lib.owm.WeatherData.Main;
import org.bitpipeline.lib.owm.WeatherData.WeatherCondition;
import org.bitpipeline.lib.owm.WeatherData.WeatherCondition.ConditionCode;
import org.bitpipeline.lib.owm.WeatherStatusResponse;

import handlers.PacketHandler;
import handlers.PokecachingClient;
import packets.GetMapPokemonInfoPacket;
import packets.GetUserItemsPacket;
import packets.Packet;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import argo.format.JsonFormatter;
import argo.format.PrettyJsonFormatter;

public class UpdateLocationTask extends AsyncTask<Object, Void, Void> implements LocationListener {
	private Location location;
	private OwmClient weatherClient;
	JsonFormatter JSON_FORMATTER = new PrettyJsonFormatter();
    @Override
    protected void onPreExecute() {
    }

    @Override
    protected Void doInBackground(Object ... params) {
    	boolean running = true;
    	weatherClient = new OwmClient();
    	while(running) {
    		try {
				if(PokecachingClient.user != null) {
					LocationManager lm = (LocationManager) params[0];
					Looper.prepare();
					// Request GPS updates. The third param is the looper to use, which defaults the the one for
					// the current thread.
					lm.requestSingleUpdate(LocationManager.GPS_PROVIDER, this, null);
					Looper.loop(); // start waiting...when this is done, we'll have the location in this.location
					
					/*LocationManager locationManager = (LocationManager) getSystemService(android.content.Context.LOCATION_SERVICE);
				    Criteria criteria = new Criteria();
			        String provider = locationManager.getBestProvider(criteria, true);
			        Location location = locationManager.getLastKnownLocation(provider);*/
			        
			        String response = null;
			        if(location!=null){
			        	double latitude = location.getLatitude();
			        	double longitude = location.getLongitude();
			        	try {
			        		Packet p = new GetMapPokemonInfoPacket(latitude,longitude);
			        		Socket communicationSocket = new Socket("94.132.186.108", 5000);
			        		if(communicationSocket != null) {
			        			communicationSocket.setSoTimeout(1000);
			        			DataOutputStream outToServer = new DataOutputStream(communicationSocket.getOutputStream());
			        			BufferedReader inFromServer = new BufferedReader(new InputStreamReader(communicationSocket.getInputStream()));
			        			
			        			String body = JSON_FORMATTER.format(p.getJson());
			        			
			        			String sentence = "GET /whatever HTTP/1.1\r\nContent-Length: " + body.length() + "\r\n\r\n" + body;
			        			outToServer.writeBytes(sentence);
			        			
			        			char[] buffer = new char[4096];
			        			int result = inFromServer.read(buffer);
			        			
			        			if(result <= 0) {
			        				communicationSocket.close();
			        				return null;
			        			}
			        			
			    				response = new String(buffer);
			    				
			    				String[] responseTokens = response.split("\\r\\n\\r\\n");
			    				
			    				if(responseTokens.length >= 2) {
			    					String responseBody = responseTokens[1].trim();
			    					PacketHandler.parse(responseBody);
			    				}
			        			
			    				communicationSocket.close();
			        		}
			    			
			    		} catch (Exception e) {
			    			System.out.println("There has been an unexpected error while performing this task: " + e.toString());
			    			e.printStackTrace();
			    		}
			        	
			        	WeatherStatusResponse weatherStatus = weatherClient.currentWeatherAroundPoint((float)latitude, (float)longitude, 1);
			        	
			        	if(weatherStatus.hasWeatherStatus()) {
			        		WeatherData data = weatherStatus.getWeatherStatus().get(0);
			        		
			        		WeatherCondition cond = data.getWeatherConditions().get(0);
			        		
			        		ConditionCode code = cond.getCode();
			        		int codeId = code.getId();
			        		
			        		Weather wth = Weather.Sunny;
			        		if(codeId >= 200 && codeId <= 522)
			        			wth = Weather.Rainy;
			        		else if (codeId <= 621 && codeId >= 600)
			        			wth = Weather.Snowy;
			        		else if (codeId == 901)
			        			wth = Weather.Rainy;
			        		
			        		PokecachingClient.currWeather = wth;
			        	}
			        }
			        
			        String username = PokecachingClient.user.getUsername();
			        if(username != null) {
			        	response = (new CommunicationTask().execute(new GetUserItemsPacket(username), CommunicationTask.MessageType.GET,Long.valueOf(1000))).get();
			        	if(response != null)
			        		PacketHandler.parse(response);
			        }
				}
				Thread.sleep(300000);
			} catch (Exception e) {
				e.printStackTrace();
			}

    	}
		return null; //This runs on a different thread
    }
   
    @Override
    protected void onProgressUpdate(Void ... values) {
    }

	@Override
	public void onLocationChanged(Location location) {
		this.location = location;
		Looper.myLooper().quit();
	}

	@Override
	public void onProviderDisabled(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		// TODO Auto-generated method stub
		
	}
}

