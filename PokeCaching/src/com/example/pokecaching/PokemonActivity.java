package com.example.pokecaching;

import java.util.ArrayList;
import java.util.List;

import handlers.PacketHandler;
import handlers.PokecachingClient;
import objects.Pokemon;
import objects.PokemonItem;
import packets.GetPokemonPacket;
import Adapters.PokemonAdapter;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.widget.ListView;

public class PokemonActivity extends Activity {
	static ListView listView;
	List<PokemonItem> pokemonItems;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_pokemon);
		
		Bundle b = getIntent().getExtras();
		int id = b.getInt("id");
		
		String response;
		try {
			response = (new CommunicationTask().execute( new GetPokemonPacket(PokecachingClient.pokedex.get(id).getIndex()), CommunicationTask.MessageType.GET,Long.valueOf(1000))).get();
			if(response != null) PacketHandler.parse(response);
		} catch (Exception e) {
			System.out.println("There has been an unexpected error.");
		}
		//String response = MainActivity.cTask.sendMessage(MessageType.GET, new GetPokemonPacket(PokecachingClient.pokedex.get(id).getIndex()), 1000);
		//String response = MainActivity.cTask.getNextResponse(1000);
				//if(response != null) PacketHandler.parse(response);
				
		//Request Pokemon from Server using id
		while(!PokecachingClient.pokedex.get(id).isLoaded()) {
			//message to wait for it to load
		}
		
		Pokemon pkmn = PokecachingClient.pokedex.get(id);
		pokemonItems = new ArrayList<PokemonItem>();
		PokemonItem item = new PokemonItem(getImageId(this,pkmn.getLargeSprite()),
											pkmn.getName(),
											pkmn.getDescription(),
											"Preferred Weather: " + pkmn.getWeather().toString(),
											"Preferred Time: " + Integer.toString(pkmn.getTime().getStartTime()) + " - " +
											Integer.toString(pkmn.getTime().getEndTime()),
											"#" + Integer.toString(pkmn.getIndex()));
		pokemonItems.add(item);
		
		listView = (ListView) findViewById(R.id.pokemonList);
		PokemonAdapter adapter = new PokemonAdapter(this,
	            R.layout.list_pokemon, pokemonItems);
		listView.setAdapter(adapter);
	}
	
	private static int getImageId(Context context, String imageName) {
	    return context.getResources().getIdentifier("drawable/" + imageName, null, context.getPackageName());
	}
}