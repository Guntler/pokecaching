package com.example.pokecaching;

import java.util.ArrayList;
import java.util.List;

import objects.CaughtPokemon;
import objects.RowItem;
import handlers.PokecachingClient;
import Adapters.RowItemArrayAdapter;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class PartyActivity extends Activity implements OnItemClickListener {
	static ListView listView;
	List<RowItem> rowItems;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_party);
		rowItems = new ArrayList<RowItem>();
		for(CaughtPokemon pkmnIdx : PokecachingClient.user.getCaught()) {
			RowItem item = new RowItem(	getImageId(this,pkmnIdx.getPokemon().getSmallSprite()),
														pkmnIdx.getNickname(),
														"Caught date: "+pkmnIdx.getDate(),
														pkmnIdx.getPokemon().getName());
			rowItems.add(item);
		}
		
		listView = (ListView) findViewById(R.id.dexGrid);
		RowItemArrayAdapter adapter = new RowItemArrayAdapter(this,
	            R.layout.list_pokedex, rowItems);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(this);
	}
	
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		
	}
	
	private static int getImageId(Context context, String imageName) {
	    return context.getResources().getIdentifier("drawable/" + imageName, null, context.getPackageName());
	}
}
