package com.example.pokecaching;

import handlers.PokecachingClient;

import java.util.ArrayList;
import java.util.List;
import objects.Pokeball;
import objects.RowItem;
import Adapters.RowItemArrayAdapter;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class InventoryActivity extends Activity implements OnItemClickListener {
	static ListView listView;
	List<RowItem> rowItems;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_inventory);
		
		rowItems = new ArrayList<RowItem>();
		for(Pokeball ball : PokecachingClient.items) {
			if(PokecachingClient.user.getInventory().get(ball) != null && PokecachingClient.user.getInventory().get(ball) != 0) {
				RowItem item = new RowItem(getImageId(this,ball.getSprite()),
											"x"+Integer.toString(PokecachingClient.user.getInventory().get(ball)),
											ball.getDescription(),
											ball.getName()
											);
				rowItems.add(item);
			}
		}
		
		listView = (ListView) findViewById(R.id.dexGrid);
		RowItemArrayAdapter adapter = new RowItemArrayAdapter(this,
	            R.layout.list_pokedex, rowItems);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(this);
	}
	
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		
	}
	
	private static int getImageId(Context context, String imageName) {
	    return context.getResources().getIdentifier("drawable/" + imageName, null, context.getPackageName());
	}
}
