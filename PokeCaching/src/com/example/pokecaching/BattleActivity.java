package com.example.pokecaching;

import handlers.PacketHandler;
import handlers.PokecachingClient;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import objects.Pokeball;
import objects.Pokemon;
import objects.PokemonItem;
import objects.RowItem;
import packets.ChangeItemsPacket;
import packets.RegisterPacket;
import Adapters.PokemonBattleAdapter;
import Adapters.RowItemArrayAdapter;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class BattleActivity extends Activity implements OnItemClickListener {
	static ListView listPkmn;
	static ListView listInventory;
	List<PokemonItem> pokemonItems;
	List<RowItem> rowItems;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_battle);
		
		boolean found = false;
		Bundle b = getIntent().getExtras();
		int id = b.getInt("id");
		
		/*
		 * Load Pokemon into battle screen
		 */
		Pokemon pkmn = null;
		for(Pokemon pkmnIdx : PokecachingClient.pokedex) {
			if(pkmnIdx.getIndex() == id) {
				pkmn = pkmnIdx;
				found = true;
				break;
			}
		}
	
		if(!found)
			return;
		
		pokemonItems = new ArrayList<PokemonItem>();
		System.out.println(pkmn.getLargeSprite());
		PokemonItem pkmnItem = new PokemonItem(getImageId(this,pkmn.getLargeSprite()),
											pkmn.getName(),
											"Oh! A wild " + pkmn.getName() + " has appeared!",
											"",
											"",
											"#" + Integer.toString(pkmn.getIndex()));
		pokemonItems.add(pkmnItem);
		
		listPkmn = (ListView) findViewById(R.id.pokemonView);
		PokemonBattleAdapter adapterPkmn = new PokemonBattleAdapter(this,
	            R.layout.list_battle_pkmn, pokemonItems);
		listPkmn.setAdapter(adapterPkmn);
		/*
		 * Load Items into battle screen
		 */
		rowItems = new ArrayList<RowItem>();
		for(Pokeball ball : PokecachingClient.items) {
			if(PokecachingClient.user.getInventory().containsKey(ball)) {
				if(PokecachingClient.user.getInventory().get(ball) != 0) {
					RowItem item = new RowItem(getImageId(this,ball.getSprite()),
												"x"+Integer.toString(PokecachingClient.user.getInventory().get(ball)),
												ball.getDescription(),
												ball.getName()
												);
					rowItems.add(item);
				}
			}
		}
		
		listInventory = (ListView) findViewById(R.id.inventoryView);
		RowItemArrayAdapter adapterInventory = new RowItemArrayAdapter(this,
	            R.layout.list_pokedex, rowItems);
		listInventory.setAdapter(adapterInventory);
		listInventory.setOnItemClickListener(this);
		
		Toast toast = Toast.makeText(this, pokemonItems.get(0).getTitle(), Toast.LENGTH_LONG);
		toast.show();
	}
	
	private static int getImageId(Context context, String imageName) {
	    return context.getResources().getIdentifier("drawable/" + imageName, null, context.getPackageName());
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		//Use Pokeball found on 'position' and calculate results
		boolean success = true;
		
		Pokeball pkBall = null;
		for(Pokeball pkballIdx : PokecachingClient.items) {
			if(pkballIdx.getName().equals(rowItems.get(position).getIndex())) {
				pkBall = pkballIdx;
				break;
			}
		}
		
		//Toast toast1 = Toast.makeText(view.getContext(), pkBall.getId(), 40);
		//toast1.show();
		
		/*Pokemon pkmn = null;
		for(Pokemon pkmnIdx : PokecachingClient.areaPkmn) {
			if(pkmnIdx.getName().equals(pokemonItems.get(0).getTitle())) {
				pkmn = pkmnIdx;
				break;
			}
		}*/
		String response;
		try {
			response = (new CommunicationTask().execute(new ChangeItemsPacket(false,PokecachingClient.user.getUsername(), pkBall.getId(), 1), CommunicationTask.MessageType.POST,Long.valueOf(1000))).get();
			if(response != null) PacketHandler.parse(response);
		} catch (Exception e) {
			Toast toast = Toast.makeText(view.getContext(), "There has been an unexpected error.", Toast.LENGTH_LONG);
			toast.show();
		}
		
		/*Toast toast2 = Toast.makeText(view.getContext(), response, 20);
		toast2.show();*/

		if(PokecachingClient.retrieveCommandResponse()) {
			PokecachingClient.user.addPokeball(-1, pkBall);
			/*float probCapture = (1/pkBall.getCatchPower()) + ((pkmn.getCatchRate()/pkBall.getCatchPower()));
			Random randomGenerator = new Random();
			float randomInt = randomGenerator.nextFloat();*/
			
			if(success) {
				Intent intent = new Intent(getApplicationContext(), CaughtActivity.class);
				intent.putExtra("name", pokemonItems.get(0).getTitle());
				startActivity(intent);
			}
			else {
				Toast toast = Toast.makeText(view.getContext(), "D'oh! So close, too!", 30);
				toast.show();
			}
		}
	}
}
