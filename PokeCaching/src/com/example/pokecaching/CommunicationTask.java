package com.example.pokecaching;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import argo.format.JsonFormatter;
import argo.format.PrettyJsonFormatter;
import packets.Packet;

public class CommunicationTask extends AsyncTask<Object, Void, String> {
	private static final JsonFormatter JSON_FORMATTER = new PrettyJsonFormatter();
	public static enum MessageType {GET, POST, PUT, DELETE};
	public static Context context;
	char[] buffer;
	
    @Override
    protected void onPreExecute() {
    }

    @Override
    protected String doInBackground(Object ... params) {
    	try {
    		Packet p = (Packet) params[0];
    		MessageType t = (MessageType) params[1];
    		Long timeout = (Long) params[2];
    		System.out.println("Creating socket");

    		//Socket communicationSocket = getConnection("94.132.186.108",5000, 500);
    		//Socket communicationSocket = getConnection("192.168.1.160",5000, 500);
    		//Socket communicationSocket = getConnection("192.168.1.184",5000, 500);
    		//Socket communicationSocket = getConnection("193.136.33.133",5000, 500);
    		//Socket communicationSocket = getConnection("172.30.9.233",5000, 500);
    		Socket communicationSocket = getConnection("172.30.10.17",5000, 500);

    		System.out.println("Created: " + communicationSocket);
    		if(communicationSocket != null) {
    			communicationSocket.setSoTimeout(timeout.intValue());
    			DataOutputStream outToServer = new DataOutputStream(communicationSocket.getOutputStream());
    			BufferedReader inFromServer = new BufferedReader(new InputStreamReader(communicationSocket.getInputStream()));
    			
    			String body = JSON_FORMATTER.format(p.getJson());
    			String type = "";
    	
    			if(t == CommunicationTask.MessageType.GET)
    				type = "GET";
    			else if(t == CommunicationTask.MessageType.POST)
    				type = "POST";
    			else if(t == CommunicationTask.MessageType.PUT)
    				type = "PUT";
    			else if(t == CommunicationTask.MessageType.DELETE)
    				type = "DELETE";
    			else return null;
    			System.out.println("Writing");
    			String sentence = type + " /whatever HTTP/1.1\r\nContent-Length: " + body.length() + "\r\n\r\n" + body;
    			outToServer.writeBytes(sentence);
    			
    			buffer = new char[4096];
    			int result = inFromServer.read(buffer);
    			
    			if(result <= 0) {
    				communicationSocket.close();
    				return null;
    			}
    			
				String response = new String(buffer);
				
				for(int i = 0; i < buffer.length; i++) //clearing the buffer
					buffer[i] = '\0';
				
				String[] responseTokens = response.split("\\r\\n\\r\\n");
				
				if(responseTokens.length >= 2) {
					String responseBody = responseTokens[1].trim();
					System.out.println("Closing socket.");
					communicationSocket.close();
					
					System.out.println("Returning.");
					return responseBody;
				}
    			
				communicationSocket.close();
    		}
			
		} catch (Exception e) {
			System.out.println("There has been an unexpected error while performing this task: " + e.toString());
			e.printStackTrace();
		}
		return null; //This runs on a different thread
    }

    @Override
    protected void onProgressUpdate(Void ... values) {
    }
    
    protected Socket getConnection(String ip, int port, int timeout) throws IOException  {
        try {
            KeyStore trustStore = KeyStore.getInstance("BKS");
            InputStream trustStoreStream = context.getResources().openRawResource(R.raw.server);
            System.out.println("Stream is " + trustStoreStream);
            trustStore.load(trustStoreStream, "keypass".toCharArray());
     
            TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            trustManagerFactory.init(trustStore);
     
            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, trustManagerFactory.getTrustManagers(), null);
            SSLSocketFactory factory = sslContext.getSocketFactory();
            SSLSocket socket = (SSLSocket) factory.createSocket();
            //SSLSocket socket = (SSLSocket) factory.createSocket(ip, port);
            //socket.setEnabledCipherSuites(SSLUtils.getCipherSuitesWhiteList(socket.getEnabledCipherSuites()));
            socket.connect(new InetSocketAddress(ip,port), timeout);
            return socket;
        } catch (GeneralSecurityException e) {
            Log.e(this.getClass().toString(), "Exception while creating context: ", e);
            throw new IOException("Could not connect to SSL Server", e);
        }
    }
}
