package com.example.pokecaching;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.ExecutionException;

import objects.Pokemon;
import objects.Weather;
import packets.GetMapPokemonInfoPacket;
import handlers.PacketHandler;
import handlers.PokecachingClient;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap.OnMyLocationChangeListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import argo.jdom.JdomParser;
import argo.jdom.JsonNode;
import argo.jdom.JsonRootNode;
import argo.saj.InvalidSyntaxException;

@SuppressLint("NewApi")
public class MapActivity extends Activity implements OnMarkerClickListener, OnMyLocationChangeListener {
	private GoogleMap mMap;
	private boolean hasStarted = false;
	private Calendar c;
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_map);
				
		mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
		mMap.setMyLocationEnabled(true);
		mMap.setOnMarkerClickListener((OnMarkerClickListener) this);
		mMap.getUiSettings().setZoomControlsEnabled(true);
		mMap.setOnMyLocationChangeListener(this);
		
	    c = Calendar.getInstance();
	}
	
	public static int getImageId(Context context, String imageName) {
	    return context.getResources().getIdentifier("drawable/" + imageName, null, context.getPackageName());
	}
	
	public void startPokedex(View view) {
		Intent intent = new Intent(this, PokedexActivity.class);
		
		startActivity(intent);
	}
	
	public void startParty(View view) {
		Intent intent = new Intent(this, PartyActivity.class);
		
		startActivity(intent);
	}
	
	public void startInventory(View view) {
		Intent intent = new Intent(this, InventoryActivity.class);
		
		startActivity(intent);
	}
	
	public void startOptions(View view) {
		Intent intent = new Intent(this, OptionsActivity.class);
		
		startActivity(intent);
	}

	@Override
	public boolean onMarkerClick(Marker marker) {
		for(Pokemon pkmnIdx : PokecachingClient.areaPkmn) {
			if(pkmnIdx.getName().equals(marker.getTitle())) {
				/*LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
			    Criteria criteria = new Criteria();
		        String provider = locationManager.getBestProvider(criteria, true);
		        Location location = locationManager.getLastKnownLocation(provider);*/
		        double mylatitude = PokecachingClient.curLocation.getLatitude();
	        	double mylongitude = PokecachingClient.curLocation.getLongitude();
				double latSup = pkmnIdx.getLocation().getLatitude()+pkmnIdx.getLocation().getRadius()+1;
				double latLow = pkmnIdx.getLocation().getLatitude()-pkmnIdx.getLocation().getRadius()-1;
				double lonSup = pkmnIdx.getLocation().getLongitude()+pkmnIdx.getLocation().getRadius()+1;
				double lonLow = pkmnIdx.getLocation().getLongitude()-pkmnIdx.getLocation().getRadius()-1;
				if((mylatitude <= latSup && mylatitude >= latLow) &&
						(mylongitude <= lonSup && mylongitude >= lonLow)) {
					boolean isTime = false;
					if(pkmnIdx.getTime().getStartTime() > pkmnIdx.getTime().getEndTime()) {
						if(pkmnIdx.getTime().getEndTime() > PokecachingClient.currHour || pkmnIdx.getTime().getStartTime() < PokecachingClient.currHour) {
							isTime = true;
						}
						else {
							Toast toast = Toast.makeText(this.findViewById(android.R.id.content).getContext(), "Couldn't see any pokemon. These pokemon only show up between " + pkmnIdx.getTime().getStartTime() + " and " + pkmnIdx.getTime().getEndTime() + " o'clock.", 10);
							toast.show();
						}
					}
					else {
						if(pkmnIdx.getTime().getEndTime() > PokecachingClient.currHour && pkmnIdx.getTime().getStartTime() < PokecachingClient.currHour) {
							isTime = true;
						}
						else {
							Toast toast = Toast.makeText(this.findViewById(android.R.id.content).getContext(), "Couldn't see any pokemon. These pokemon only show up between " + pkmnIdx.getTime().getStartTime() + " and " + pkmnIdx.getTime().getEndTime() + " o'clock.", 10);
							toast.show();
						}
					}
					
					if(isTime) {
						
						if(PokecachingClient.currWeather == pkmnIdx.getWeather()) {
							Intent intent = new Intent(this, BattleActivity.class);
							intent.putExtra("id", pkmnIdx.getIndex());
							startActivity(intent);
						}
						else {
							String wth;
							if(pkmnIdx.getWeather() == Weather.Sunny)
								wth = "sunny";
							else if(pkmnIdx.getWeather() == Weather.Rainy)
								wth = "rainy";
							else
								wth = "snowy";
							Toast toast = Toast.makeText(this.findViewById(android.R.id.content).getContext(), "Couldn't see any pokemon. These pokemon only show up on " + wth + " weather.", 10);
							toast.show();
						}
					}
				}
			}
		}
		return false;
	}

	@Override
	public void onMyLocationChange(Location location) {
		// Removes all markers, overlays, and polylines from the map.
	    mMap.clear();
		
		PokecachingClient.curLocation = location;
		CameraUpdate myLoc = CameraUpdateFactory.newCameraPosition(
	            new CameraPosition.Builder().target(new LatLng(location.getLatitude(),
	            		location.getLongitude())).zoom(13).build());
	    mMap.moveCamera(myLoc);
	    mMap.setOnMyLocationChangeListener(null);

	    if(!hasStarted) {
	    	PokecachingClient.iTask = new UpdateItemsTask();
		    
		    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
		    	PokecachingClient.iTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		    else
		    	PokecachingClient.iTask.execute();
		    
		    hasStarted = true;
	    }

		int hours = c.get(Calendar.HOUR_OF_DAY);
		int minutes = c.get(Calendar.MINUTE);
		int differenceMinutes = minutes - PokecachingClient.prevMinute;
		
		//Check if five minutes have passed
		if( differenceMinutes >= 1)
		{
			checkWeather(location);
			PokecachingClient.prevMinute = minutes;
		}
		//An hour has passed between checks, so the previous time will be much larger than the current time
		else if(differenceMinutes < 0) {
			checkWeather(location);
			PokecachingClient.prevMinute = minutes;
		}
		
		System.out.println("Weather: " + PokecachingClient.currWeather.toString());
		
		PokecachingClient.currHour = hours;

	    String response;
		try {
			response = (new CommunicationTask().execute(new GetMapPokemonInfoPacket(location.getLatitude(),location.getLongitude()), CommunicationTask.MessageType.GET,Long.valueOf(1000))).get();
			if(response != null) {PacketHandler.parse(response);}
		} catch (Exception e) {
			System.out.println("There has been an unexpected error.");
		}
		
		for(Pokemon pkmnIdx : PokecachingClient.areaPkmn) {
			mMap.addMarker(new MarkerOptions()
	        				.position(new LatLng(pkmnIdx.getLocation().getLatitude(), pkmnIdx.getLocation().getLongitude()))
	        				.title(pkmnIdx.getName())
	        				.draggable(false)
	        				.icon(BitmapDescriptorFactory.fromResource(getImageId(this, pkmnIdx.getSmallSprite()))));
		}
	}
	
	public void checkWeather(Location location) {
		CheckWeatherTask task = new CheckWeatherTask();
    	int codeId;
		try {
			codeId = task.execute(location.getLatitude(),location.getLongitude()).get();
		
    		
			System.out.println("Current weatherId is: " + codeId);
		
        	Weather wth = Weather.Sunny;
			if(codeId >= 200 && codeId <= 522)
				wth = Weather.Rainy;
			else if (codeId <= 621 && codeId >= 600)
				wth = Weather.Snowy;
			else if (codeId == 901)
				wth = Weather.Rainy;
		
			PokecachingClient.currWeather = wth;
		} catch (InterruptedException e) {e.printStackTrace();}
		catch (ExecutionException e) {e.printStackTrace();}
	}
	
	class CheckWeatherTask extends AsyncTask<Double,Void,Integer> {
		public JdomParser parser = new JdomParser();
	    public JsonRootNode packetBody;
	    
		@Override
		protected Integer doInBackground(Double... params) {
			HttpURLConnection connection = null;
	         try {
	             URL url = new URL("http://api.openweathermap.org/data/2.5/weather?lat=" + params[0]
	         																+ "&lon=" + params[1]);
	             connection = (HttpURLConnection) url.openConnection();
	             connection.connect();
	             connection.getInputStream();
	             BufferedReader in = new BufferedReader(
	                     new InputStreamReader(
	                     connection.getInputStream()));
	             String decodedString = in.readLine();
	             
	             packetBody = parser.parse(decodedString);
	             List<JsonNode> nodes = packetBody.getArrayNode("weather");
	             
	             int codeId = 0;
	     		for(int i=0; i<nodes.size(); i++) {
	     			codeId = Integer.parseInt(nodes.get(i).getNumberValue("id"));
	     		}

	     		return codeId;

	         } catch (MalformedURLException e1) {e1.printStackTrace();}
	         catch (IOException e1) {e1.printStackTrace();}
	         catch (InvalidSyntaxException e) {e.printStackTrace();}
	         finally {
	             if(null != connection) { connection.disconnect(); }
	         }
			return 801;
		}
	 }
}
