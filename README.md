# Pokecaching Location Based Game #

Pokecaching is location based game for smartphones combining the collection aspects of Pokemon with the exploration aspects of geocaching. The client was written in Java for the Android platform using the Google Maps API. The server was written in NodeJS using SQLite as a database system.

Developed by João Marinheiro and Daniel Pereira for the Distributed Systems course at FEUP.